DROP SCHEMA IF EXISTS webavio;
CREATE SCHEMA webavio DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE webavio;

CREATE TABLE korisnik (
	id INT AUTO_INCREMENT,
	korisnickoIme VARCHAR(30) NOT NULL, 
	lozinka VARCHAR(30) NOT NULL, 
    datumRegistracije datetime not null,
	uloga ENUM('KORISNIK', 'ADMIN') NOT NULL DEFAULT 'KORISNIK', 
    blokiran boolean not null,
    active boolean not null,
    PRIMARY KEY(id)
);

insert into korisnik (korisnickoIme, lozinka, datumRegistracije, uloga, blokiran, active) values
('milos','123',now(),'ADMIN',false,true);
insert into korisnik (korisnickoIme, lozinka, datumRegistracije, uloga, blokiran, active) values
('raca','123',now(),'KORISNIK',false,true);
insert into korisnik (korisnickoIme, lozinka, datumRegistracije, uloga, blokiran, active) values
('ilija','123',now(),'KORISNIK',false,true);

CREATE TABLE aerodrom (
	id INT AUTO_INCREMENT,
	naziv VARCHAR(30) NOT NULL, 
    active boolean not null,
    PRIMARY KEY(id)
);
insert into aerodrom (naziv, active) values
('Lacarak Aerodrom',true);
insert into aerodrom (naziv, active) values
('Frankfurt Aerodrom',true);
insert into aerodrom (naziv, active) values
('Kambera Aerodrom',true);
insert into aerodrom (naziv, active) values
('Madrid Aerodrom',true);
insert into aerodrom (naziv, active) values
('Moskva Aerodrom',true);

CREATE TABLE let (
	id INT AUTO_INCREMENT,
	broj VARCHAR(30) NOT NULL, 
	datumPolaska datetime NOT NULL, 
    datumDolaska datetime not null,
	polazniAerodrom varchar(30) not null, 
    dolazniAerodrom varchar(30) not null, 
    brojSedista int not null,
    cena DECIMAL(10, 2) not null,
    active boolean not null,
    PRIMARY KEY(id)
);
INSERT INTO let (broj, datumPolaska, datumDolaska, polazniAerodrom, dolazniAerodrom, brojSedista, cena, active) 
VALUES ('101','2019-01-08 02:15:33', '2019-01-08 04:43:33', 'Frankfurt Aerodrom', 'Kambera Aerodrom', 77, 777, true);
INSERT INTO let (broj, datumPolaska, datumDolaska, polazniAerodrom, dolazniAerodrom, brojSedista, cena, active) 
VALUES ('102', '2019-01-09 05:00:33', '2019-01-09 08:53:33', 'Kambera Aerodrom', 'Frankfurt Aerodrom', 33, 999, true);
INSERT INTO let (broj, datumPolaska, datumDolaska, polazniAerodrom, dolazniAerodrom, brojSedista, cena, active) 
VALUES ('103', '2019-10-09 03:15:33', '2019-01-09 05:15:33', 'Kambera Aerodrom', 'Lacarak Aerodrom', 50, 700, true);
INSERT INTO let (broj, datumPolaska, datumDolaska, polazniAerodrom, dolazniAerodrom, brojSedista, cena, active) 
VALUES ('104', '2019-10-09 03:15:33', '2019-01-09 05:15:33', 'Frankfurt Aerodrom', 'Lacarak Aerodrom', 65, 888, true);
INSERT INTO let (broj, datumPolaska, datumDolaska, polazniAerodrom, dolazniAerodrom, brojSedista, cena, active) 
VALUES ('105', '2019-10-10 05:22:33', '2019-01-10 08:15:33', 'Lacarak Aerodrom', 'Moskva Aerodrom', 78, 660, true);
INSERT INTO let (broj, datumPolaska, datumDolaska, polazniAerodrom, dolazniAerodrom, brojSedista, cena, active) 
VALUES ('106', '2019-01-11 09:32:33', '2019-01-11 12:15:33', 'Moskva Aerodrom', 'Lacarak Aerodrom', 99, 1000, true);
   
INSERT INTO let (broj, datumPolaska, datumDolaska, polazniAerodrom, dolazniAerodrom, brojSedista, cena, active) 
VALUES ('107','2019-02-22 02:15:33', '2019-02-22 04:43:33', 'Frankfurt Aerodrom', 'Kambera Aerodrom', 77, 777, true);
INSERT INTO let (broj, datumPolaska, datumDolaska, polazniAerodrom, dolazniAerodrom, brojSedista, cena, active) 
VALUES ('108', '2019-02-23 05:00:33', '2019-02-23 08:53:33', 'Kambera Aerodrom', 'Frankfurt Aerodrom', 66, 999, true);
INSERT INTO let (broj, datumPolaska, datumDolaska, polazniAerodrom, dolazniAerodrom, brojSedista, cena, active) 
VALUES ('109', '2019-02-23 03:15:33', '2019-02-23 05:15:33', 'Moskva Aerodrom', 'Lacarak Aerodrom', 50, 700, true);
INSERT INTO let (broj, datumPolaska, datumDolaska, polazniAerodrom, dolazniAerodrom, brojSedista, cena, active) 
VALUES ('110', '2019-02-24 03:15:33', '2019-02-24 05:15:33', 'Frankfurt Aerodrom', 'Lacarak Aerodrom', 65, 888, true);
INSERT INTO let (broj, datumPolaska, datumDolaska, polazniAerodrom, dolazniAerodrom, brojSedista, cena, active) 
VALUES ('111', '2019-02-22 07:22:33', '2019-02-22 10:15:33', 'Lacarak Aerodrom', 'Moskva Aerodrom', 78, 660, true);
INSERT INTO let (broj, datumPolaska, datumDolaska, polazniAerodrom, dolazniAerodrom, brojSedista, cena, active) 
VALUES ('112', '2019-02-23 09:32:33', '2019-02-23 12:15:33', 'Moskva Aerodrom', 'Lacarak Aerodrom', 99, 1000, true);




CREATE TABLE rezervacija (
    id int NOT NULL AUTO_INCREMENT,
    polazniLetId int,
    povratniLetId int,
    sedistePolazniLet varchar(50),
    sedistePovratniLet varchar(50),
    datumRezervacije DATETIME,
    datumProdaje DATETIME,
    korisnikId int,
    PRIMARY KEY (id),
    FOREIGN KEY (polazniLetId) REFERENCES let(id),
    FOREIGN KEY (povratniLetId) REFERENCES let(id),
    FOREIGN KEY (korisnikId) REFERENCES korisnik(id),
    imePutnika varchar(30) not null,
    prezimePutnika varchar(30) not null,
    active boolean not null 
);

INSERT INTO rezervacija (polazniLetId, povratniLetId, sedistePolazniLet, sedistePovratniLet,  datumRezervacije, korisnikId, imePutnika, prezimePutnika, active) 
VALUES (1, 2, '1', '2', '2019-01-06 02:00:33', 2, 'Milos', 'Tomac', true);
INSERT INTO rezervacija (polazniLetId, povratniLetId, sedistePolazniLet, sedistePovratniLet,  datumProdaje, korisnikId, imePutnika, prezimePutnika, active) 
VALUES (5, 6, '3', '5', '2019-01-09 02:00:33', 2, 'Lenka', 'Grujicic', true);
INSERT INTO rezervacija (polazniLetId, sedistePolazniLet, datumProdaje, korisnikId, imePutnika, prezimePutnika, active) 
VALUES (4, '3', '2019-01-07 02:00:33', 2, 'Lenka', 'Grujicic', true);
INSERT INTO rezervacija (polazniLetId, sedistePolazniLet, datumRezervacije, korisnikId, imePutnika, prezimePutnika, active) 
VALUES (4, '5', '2019-01-07 12:00:33', 3, 'Ilija', 'Tomac', true);
INSERT INTO rezervacija (polazniLetId, sedistePolazniLet,  datumProdaje, korisnikId, imePutnika, prezimePutnika, active) 
VALUES (3, '10', '2019-01-07 12:00:33', 3, 'Ilija', 'Tomac', true);
INSERT INTO rezervacija (polazniLetId, povratniLetId, sedistePolazniLet, sedistePovratniLet,  datumProdaje, korisnikId, imePutnika, prezimePutnika, active) 
VALUES (2, 1, '5', '8', '2019-01-06 12:00:33', 3, 'Rajko', 'Teodosic', true);
INSERT INTO rezervacija (polazniLetId, sedistePolazniLet,  datumRezervacije, korisnikId, imePutnika, prezimePutnika, active) 
VALUES (3, '22', '2019-01-07 12:00:33', 2, 'Rajko', 'Teodosic', true);
INSERT INTO rezervacija (polazniLetId, povratniLetId, sedistePolazniLet, sedistePovratniLet,  datumProdaje, korisnikId, imePutnika, prezimePutnika, active) 
VALUES (2, 1, 'S17', 'S19', '2019-01-06 12:00:33', 2, 'Rajko', 'Teodosic', true);

