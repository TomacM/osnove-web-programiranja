package model;


public class Rezervacija {

	
	private String id;
	private String polazniLet;
	private String povratniLet;
	private String sedistePolazniLet;
	private String sedistePovratniLet;
	private String datumRezervacije;
	private String datumProdaje;
	private Korisnik korisnik;
	private String imePutnika;
	private String prezimePutnika;
	private boolean active;
	
	public Rezervacija() {
		this.active = true;
	}

	public Rezervacija(String id, String polazniLet, String povratniLet, String sedistePolazniLet, String sedistePovratniLet,
			String datumRezervacije, String datumProdaje, Korisnik korisnik, String imePutnika, String prezimePutnika,
			boolean active) {
		super();
		this.id = id;
		this.polazniLet = polazniLet;
		this.povratniLet = povratniLet;
		this.sedistePolazniLet = sedistePolazniLet;
		this.sedistePovratniLet = sedistePovratniLet;
		this.datumRezervacije = datumRezervacije;
		this.datumProdaje = datumProdaje;
		this.korisnik = korisnik;
		this.imePutnika = imePutnika;
		this.prezimePutnika = prezimePutnika;
		this.active = active;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPolazniLet() {
		return polazniLet;
	}

	public void setPolazniLet(String polazniLet) {
		this.polazniLet = polazniLet;
	}

	public String getPovratniLet() {
		return povratniLet;
	}

	public void setPovratniLet(String povratniLet) {
		this.povratniLet = povratniLet;
	}

	public String getSedistePolazniLet() {
		return sedistePolazniLet;
	}

	public void setSedistePolazniLet(String sedistePolazniLet) {
		this.sedistePolazniLet = sedistePolazniLet;
	}

	public String getSedistePovratniLet() {
		return sedistePovratniLet;
	}

	public void setSedistePovratniLet(String sedistePovratniLet) {
		this.sedistePovratniLet = sedistePovratniLet;
	}

	public String getDatumRezervacije() {
		return datumRezervacije;
	}

	public void setDatumRezervacije(String datumRezervacije) {
		this.datumRezervacije = datumRezervacije;
	}

	public String getDatumProdaje() {
		return datumProdaje;
	}

	public void setDatumProdaje(String datumProdaje) {
		this.datumProdaje = datumProdaje;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	public String getImePutnika() {
		return imePutnika;
	}

	public void setImePutnika(String imePutnika) {
		this.imePutnika = imePutnika;
	}

	public String getPrezimePutnika() {
		return prezimePutnika;
	}

	public void setPrezimePutnika(String prezimePutnika) {
		this.prezimePutnika = prezimePutnika;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	
	
	
}
