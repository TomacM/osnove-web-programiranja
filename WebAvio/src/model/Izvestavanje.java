package model;

public class Izvestavanje {

	private String nazivAero;
	private int brojLetova;
	private int brojProdatihKarata;
	private double cena;
	
	public Izvestavanje() {
		
	}

	public Izvestavanje(String nazivAero, int brojLetova, int brojProdatihKarata, double cena) {
		super();
		this.nazivAero = nazivAero;
		this.brojLetova = brojLetova;
		this.brojProdatihKarata = brojProdatihKarata;
		this.cena = cena;
	}

	public String getNazivAero() {
		return nazivAero;
	}

	public void setNazivAero(String nazivAero) {
		this.nazivAero = nazivAero;
	}

	public int getBrojLetova() {
		return brojLetova;
	}

	public void setBrojLetova(int brojLetova) {
		this.brojLetova = brojLetova;
	}

	public int getBrojProdatihKarata() {
		return brojProdatihKarata;
	}

	public void setBrojProdatihKarata(int brojProdatihKarata) {
		this.brojProdatihKarata = brojProdatihKarata;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}
	
	
}
