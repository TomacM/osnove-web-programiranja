package model;

public class Let {
	private String id;
	private String broj;
	private String datumPolaska;
	private String datumDolaska;
	private String polazniAerodrom;
	private String dolazniAerodrom;
	private int brojSedista;
	private double cena;
	private boolean active;

	public Let() {
		this.active=true;
	}

	public Let(String id, String broj, String datumPolaska, String datumDolaska, String polazniAerodrom,
			String dolazniAerodrom, int brojSedista, double cena, boolean active) {
		super();
		this.id = id;
		this.broj = broj;
		this.datumPolaska = datumPolaska;
		this.datumDolaska = datumDolaska;
		this.polazniAerodrom = polazniAerodrom;
		this.dolazniAerodrom = dolazniAerodrom;
		this.brojSedista = brojSedista;
		this.cena = cena;
		this.active = active;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBroj() {
		return broj;
	}

	public void setBroj(String broj) {
		this.broj = broj;
	}

	public String getDatumPolaska() {
		return datumPolaska;
	}

	public void setDatumPolaska(String datumPolaska) {
		this.datumPolaska = datumPolaska;
	}

	public String getDatumDolaska() {
		return datumDolaska;
	}

	public void setDatumDolaska(String datumDolaska) {
		this.datumDolaska = datumDolaska;
	}

	public String getPolazniAerodrom() {
		return polazniAerodrom;
	}

	public void setPolazniAerodrom(String polazniAerodrom) {
		this.polazniAerodrom = polazniAerodrom;
	}

	public String getDolazniAerodrom() {
		return dolazniAerodrom;
	}

	public void setDolazniAerodrom(String dolazniAerodrom) {
		this.dolazniAerodrom = dolazniAerodrom;
	}

	public int getBrojSedista() {
		return brojSedista;
	}

	public void setBrojSedista(int brojSedista) {
		this.brojSedista = brojSedista;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

}
