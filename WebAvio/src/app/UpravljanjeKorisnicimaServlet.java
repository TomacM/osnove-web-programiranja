package app;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.KorisnikDAO;
import model.Korisnik;



public class UpravljanjeKorisnicimaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String loggedInUserId = (String) request.getSession().getAttribute("loggedInUserId");
		if (loggedInUserId == null) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		Korisnik loggedInUser = KorisnikDAO.get(loggedInUserId);
		if (loggedInUser == null) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		if(loggedInUser.getUloga().name().equals("KORISNIK")){
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		
		List<Korisnik> listaKorisnika = KorisnikDAO.getAll();
		
		List<String> roles = new ArrayList<String>();
		
		for(Korisnik kor : listaKorisnika) {
			if(roles.contains(kor.getUloga().toString())) {
				continue;
			}
			roles.add(kor.getUloga().toString());
		}
		
		String korIme = request.getParameter("korImeFilter");
		korIme = (korIme != null? korIme: "");
		
		
		String uloga = request.getParameter("ulogaFilter");
		uloga = (uloga != null ? uloga : "");
		
		System.out.println(uloga);
		List<Korisnik> filteredKorisnici = KorisnikDAO.getAll(korIme, uloga);
		List<Korisnik> rastuceSortKorIme = KorisnikDAO.getKorImeAsc(korIme, uloga);
		List<Korisnik> opadajuceSortKorIme = KorisnikDAO.getKorImeDesc(korIme, uloga);
		List<Korisnik> rastuceSortUloga = KorisnikDAO.getUlogaAsc(korIme, uloga);
		List<Korisnik> opadajuceSortUloga = KorisnikDAO.getUlogaDesc(korIme, uloga);
		
		Map<String, Object> data = new LinkedHashMap<>();
		data.put("filteredKorisnici", filteredKorisnici);
		data.put("rastuceSortKorIme", rastuceSortKorIme);
		data.put("opadajuceSortKorIme", opadajuceSortKorIme);
		data.put("rastuceSortUloga", rastuceSortUloga);
		data.put("opadajuceSortUloga", opadajuceSortUloga);
		data.put("roles", roles);
		
		request.setAttribute("data", data);
		request.getRequestDispatcher("./SuccessServlet").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String loggedInUserId = (String) request.getSession().getAttribute("loggedInUserId");
		if (loggedInUserId == null) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		Korisnik loggedInUser = KorisnikDAO.get(loggedInUserId);
		if (loggedInUser == null) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		
		try {
			String action = request.getParameter("action");
			switch (action) {
				case "add": {
					break;
				}
				case "delete": {
					break;
				}
			}	
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		request.getRequestDispatcher("./SuccessServlet").forward(request, response);
	}

}
