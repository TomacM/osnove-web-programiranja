package app;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.KorisnikDAO;
import dao.LetDAO;
import dao.RezervacijaDAO;
import model.Korisnik;
import model.Let;
import model.Rezervacija;
import model.Uloga;


public class PojedinacniLetServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String loggedInUserId = (String) request.getSession().getAttribute("loggedInUserId");
		if (loggedInUserId == null) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		Korisnik loggedInUser = KorisnikDAO.get(loggedInUserId);
		if (loggedInUser == null) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		String id = request.getParameter("id");
		System.out.println(id);
		Let lett = LetDAO.get(id);

		List<String> polazniAerodromi = new ArrayList<>();
		for(Let l : LetDAO.getAll()) {
			polazniAerodromi.add(l.getPolazniAerodrom());
		}
		List<String> dolazniAerodromi = new ArrayList<>();
		for(Let l : LetDAO.getAll()) {
			dolazniAerodromi.add(l.getDolazniAerodrom());
		}
		int slobodnaSedista = 0;
		for(Rezervacija rez : RezervacijaDAO.getAll()) {
			if(lett.getId().equals(rez.getPolazniLet())) {
				slobodnaSedista++;
			}
			if(lett.getId().equals(rez.getPovratniLet())) {
				slobodnaSedista++;
			}
		}
		slobodnaSedista = lett.getBrojSedista() - slobodnaSedista;
		
		
		
		
		Map<String, Object> data = new LinkedHashMap<>();
		data.put("lett", lett);
		data.put("slobodnaSedista", slobodnaSedista);
		data.put("loggedInUserRole", loggedInUser.getUloga());
		data.put("loggedInUser", loggedInUser);
		data.put("polazniAerodromi", polazniAerodromi);
		data.put("dolazniAerodromi", dolazniAerodromi);
		
		
		request.setAttribute("data", data);
		request.getRequestDispatcher("./SuccessServlet").forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String loggedInUserId = (String) request.getSession().getAttribute("loggedInUserId");
		if (loggedInUserId == null) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		
		Korisnik loggedInUser = KorisnikDAO.get(loggedInUserId);
		if (loggedInUser == null) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		if(loggedInUser.getUloga().name().equals("KORISNIK")){
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		if (loggedInUser.getUloga() != Uloga.ADMIN) {
			request.getRequestDispatcher("./UnauthorizedServlet").forward(request, response);
			return;
		}
		List<String> polazniAerodromi = new ArrayList<>();
		for(Let l : LetDAO.getAll()) {
			polazniAerodromi.add(l.getPolazniAerodrom());
		}
		List<String> dolazniAerodromi = new ArrayList<>();
		for(Let l : LetDAO.getAll()) {
			dolazniAerodromi.add(l.getDolazniAerodrom());
		}
		List<Let> letovi = LetDAO.getAll();
		
		Map<String, Object> data = new LinkedHashMap<>();
		data.put("letovi", letovi);
		data.put("polazniAerodromi", polazniAerodromi);
		data.put("dolazniAerodromi", dolazniAerodromi);
		
		
		try {
			String action = request.getParameter("action");
			switch (action) {
				case "add": {
					String broj = request.getParameter("broj");
					String datumPolaska = request.getParameter("datumPolaska");
					String datumDolaska = request.getParameter("datumDolaska");
					String polazniAerodrom = request.getParameter("polazniAerodrom");
					String dolazniAerodrom = request.getParameter("dolazniAerodrom");
					int brojSedista = Integer.parseInt(request.getParameter("brojSedista"));
					brojSedista = (brojSedista >0?brojSedista: 99);
					
					double cena = Double.parseDouble(request.getParameter("cena"));
					cena = (cena > 0? cena: 99999999.00);
					
					List<String> polazni = new ArrayList<>();
					List<String> dolazni = new ArrayList<>();
					
					for(Let l : letovi) {
						polazni.add(l.getPolazniAerodrom());
						dolazni.add(l.getDolazniAerodrom());
							if(broj.equals(l.getBroj())) {
								request.getRequestDispatcher("./FailureServlet").forward(request, response);
								return;
							}
					}
					
					Let let = new Let("",broj,datumPolaska,datumDolaska,polazniAerodrom,dolazniAerodrom,brojSedista,cena, true);
					LetDAO.add(let);
					break;
				}
				case "update": {
					String id = request.getParameter("id");
					String datumDolaska = request.getParameter("datumDolaska");
					String datumPolaska = request.getParameter("datumPolaska");
					String polazniAerodrom = request.getParameter("polazniAerodrom");
					String dolazniAerodrom = request.getParameter("dolazniAerodrom");
					String brojSedista = request.getParameter("brojSedista");
					String cena = request.getParameter("cena");
					
			
					
					
					Let let = LetDAO.get(id);
					let.setDatumPolaska(datumPolaska);
					let.setDatumDolaska(datumDolaska);
					let.setPolazniAerodrom(polazniAerodrom);
					let.setDolazniAerodrom(dolazniAerodrom);
					let.setBrojSedista(Integer.parseInt(brojSedista));
					let.setCena(Double.parseDouble(cena));
					
					if(id == null) {
						request.getRequestDispatcher("./FailureServlet").forward(request, response);
						return;
					}
					LetDAO.update(let);
					break;
				}
				case "delete": {
					String id = request.getParameter("id");
					if(!LetDAO.deleteProdateLetova(id)) {
						LetDAO.deleteRezervacijeLetova(id);
						LetDAO.deleteLetove(id);
					}
					else {
						LetDAO.deleteRezervacijeLetova(id);
					}
					break;
				}
				case "sort":{
					String id = request.getParameter("id");
					List<Rezervacija> descDatumProdaje = RezervacijaDAO.getDatumProdajeDesc(id);
					
					data.put("descDatumProdaje",descDatumProdaje);
					break;
				}
			}
			
			request.getRequestDispatcher("./SuccessServlet").forward(request, response);
		} catch (Exception ex) {
			ex.printStackTrace();
			request.getRequestDispatcher("./FailureServlet").forward(request, response);
		}
	}

}

	