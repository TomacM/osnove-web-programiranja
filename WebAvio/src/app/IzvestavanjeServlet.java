package app;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.IzvestavanjeDAO;
import dao.KorisnikDAO;
import model.Korisnik;
import model.Izvestavanje;


public class IzvestavanjeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String loggedInUserId = (String) request.getSession().getAttribute("loggedInUserId");
		if (loggedInUserId == null) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		Korisnik loggedInUser = KorisnikDAO.get(loggedInUserId);
		if (loggedInUser == null) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		if(loggedInUser.isBlokiran()==true) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		if(loggedInUser.getUloga().name().equals("KORISNIK")){
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		
		String lowDatumDTFilter = request.getParameter("lowDatumDTFilter");
		String highDatumDTFilter = request.getParameter("highDatumDTFilter");
		
		List<Izvestavanje> izvestavanje = IzvestavanjeDAO.getAll(lowDatumDTFilter, highDatumDTFilter);

		
		Map<String, Object> data = new LinkedHashMap<>();
		data.put("izvestavanje", izvestavanje);
		
		request.setAttribute("data", data);
		request.getRequestDispatcher("./SuccessServlet").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
