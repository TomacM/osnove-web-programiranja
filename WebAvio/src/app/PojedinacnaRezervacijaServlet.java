package app;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.KorisnikDAO;
import dao.LetDAO;
import dao.RezervacijaDAO;
import model.Korisnik;
import model.Let;
import model.Rezervacija;


public class PojedinacnaRezervacijaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String loggedInUserId = (String) request.getSession().getAttribute("loggedInUserId");
		
		if (loggedInUserId == null) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		Korisnik loggedInUser = KorisnikDAO.get(loggedInUserId);
		if (loggedInUser == null) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}

		String id = request.getParameter("id");
		Rezervacija rez = RezervacijaDAO.get(id);
		String idRez = rez.getKorisnik().getId();
		
		if(loggedInUser.getUloga().name().equals("KORISNIK") && !(loggedInUser.getId().equals(idRez))) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		
		Korisnik korisnik = KorisnikDAO.get(loggedInUserId);
		String uloga = korisnik.getUloga().name();
		
		if(uloga.equals("KORISNIK")) {
			if(!korisnik.getId().equals(rez.getKorisnik().getId())) {
				request.getRequestDispatcher("./LogoutServlet").forward(request, response);
				return;
			}
			
		}
		
		List<Let> letovi = LetDAO.getAll();
		double cena = 0;
		for(Let let : letovi) {
			if(rez.getPovratniLet() == null) {
				
				if(rez.getPolazniLet().equals(let.getId())) {
					cena = let.getCena();
					break;
				}	
			}
			else {
				if(rez.getPolazniLet().equals(let.getId())) {
					cena = cena + let.getCena();
				}
				
				if(rez.getPovratniLet().equals(let.getId())) {
					cena = cena + let.getCena();
				}
			}
			
		}
		
		Let lett = new Let();
		lett.setCena(cena);
		cena = 0;

		int polazniBrSedista = 0;
		int povratniBrSedista = 0;
		List<String> polazniSedista = new ArrayList<String>();
		List<String> povratniSedista = new ArrayList<String>();
		
		for (Let let : letovi) {
			if(rez.getPovratniLet() == null) {
				if(rez.getPolazniLet().equals(let.getId())) {
					polazniBrSedista = let.getBrojSedista();
					break;
				}
			}
			else {
				if(rez.getPolazniLet().equals(let.getId())) {
					polazniBrSedista = let.getBrojSedista();
				}
				
				if(rez.getPovratniLet().equals(let.getId())) {
					povratniBrSedista = let.getBrojSedista();
				}
			}
		}
		
		for (int i = 1; i <= polazniBrSedista; i++) {
			polazniSedista.add(String.valueOf(i));
		}
		
		if(povratniBrSedista != 0 ) {
			for (int i = 1; i <= povratniBrSedista; i++) {
				povratniSedista.add(String.valueOf(i));
			}
		}
		List<Rezervacija> rezervacije = RezervacijaDAO.getAll();
		
		for(Rezervacija rezerv : rezervacije) {
			if(rez.getPovratniLet() == null) {
				if(rez.getPolazniLet().equals(rezerv.getPolazniLet())) {
					polazniSedista.remove(rezerv.getSedistePolazniLet());
				}
				if(rez.getPolazniLet().equals(rezerv.getPovratniLet())) {
					polazniSedista.remove(rezerv.getSedistePovratniLet());
				}
			}
			else {
				if(rez.getPolazniLet().equals(rezerv.getPolazniLet())) {
					polazniSedista.remove(rezerv.getSedistePolazniLet());
				}
				if(rez.getPolazniLet().equals(rezerv.getPovratniLet())) {
					polazniSedista.remove(rezerv.getSedistePovratniLet());
				}
				if(rez.getPovratniLet().equals(rezerv.getPolazniLet())) {
					povratniSedista.remove(rezerv.getSedistePolazniLet());
				}
				if(rez.getPovratniLet().equals(rezerv.getPovratniLet())) {
					povratniSedista.remove(rezerv.getSedistePovratniLet());
				}
			}
		}
		
		
		Map<String, Object> data = new LinkedHashMap<>();
		data.put("rez", rez);
		data.put("lett", lett);
		data.put("loggedInUserRole", loggedInUser.getUloga());
		data.put("loggedInUser", loggedInUser);
		data.put("polazniSedista", polazniSedista);
		data.put("povratniSedista", povratniSedista);

		request.setAttribute("data", data);
		request.getRequestDispatcher("./SuccessServlet").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String loggedInUserId = (String) request.getSession().getAttribute("loggedInUserId");
		if (loggedInUserId == null) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		Korisnik loggedInUser = KorisnikDAO.get(loggedInUserId);
		if (loggedInUser == null) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		Map<String, Object> data = new LinkedHashMap<>();
		data.put("loggedInUser", loggedInUser);
		try {
			String action = request.getParameter("action");
			switch (action) {
				case "kupovinaKarte": {
					String id = request.getParameter("id");
					
					java.text.SimpleDateFormat sdf = 
						     new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Calendar now = Calendar.getInstance();
					
					RezervacijaDAO.kupovinaKartePrekoRezervisane(sdf.format(now.getTime()).toString(), id);
					
					break;
				}
				case "update": {
					
					String sedistePolazni = request.getParameter("sedistePolazni");
					String sedistePovratni = request.getParameter("sedistePovratni");
					String imePutnika = request.getParameter("imePutnika");
					String prezimePutnika = request.getParameter("prezimePutnika");
					
					if(sedistePovratni.equals("")) {
						sedistePovratni = null;
					}
					
					String id = request.getParameter("id");
					Rezervacija rez = RezervacijaDAO.get(id); 
					
					rez.setSedistePolazniLet(sedistePolazni);
					rez.setSedistePovratniLet(sedistePovratni);
					rez.setImePutnika(imePutnika);
					rez.setPrezimePutnika(prezimePutnika);
					
					if(id == null) {
						request.getRequestDispatcher("./FailureServlet").forward(request, response);
						return;
					}
					RezervacijaDAO.updateRezervacija(rez);
					break;
				}
				case "kupovinaU3faze":{
					String polazniLetID = request.getParameter("polazniLetID");
					String povratniLetID = request.getParameter("povratniLetID");
					String sedistePolazni = request.getParameter("sedistePolazni");
					String sedistePovratni = request.getParameter("sedistePovratni");
					String imePutnika = request.getParameter("imePutnika");
					String prezimePutnika = request.getParameter("prezimePutnika");
					java.text.SimpleDateFormat sdf = 
						     new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Calendar now = Calendar.getInstance();
					Korisnik kor = new Korisnik();
					kor.setId(loggedInUserId);
					String datumRezervacije = null;
					String datumProdaje = sdf.format(now.getTime()).toString();
					
					if(povratniLetID.equals("null")) {
						povratniLetID = null;
						sedistePovratni = null;
					}
					boolean active = true;
					
					Rezervacija kup = new Rezervacija("",polazniLetID, povratniLetID, sedistePolazni, sedistePovratni,datumRezervacije,datumProdaje,kor,
							imePutnika,prezimePutnika,active);
					RezervacijaDAO.kupovinaU3faze(kup);
					break;
				}
				case "rezervacijaU3faze":{
					String polazniLetID = request.getParameter("polazniLetID");
					String povratniLetID = request.getParameter("povratniLetID");
					String sedistePolazni = request.getParameter("sedistePolazni");
					String sedistePovratni = request.getParameter("sedistePovratni");
					String imePutnika = request.getParameter("imePutnika");
					String prezimePutnika = request.getParameter("prezimePutnika");
					java.text.SimpleDateFormat sdf = 
						     new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Calendar now = Calendar.getInstance();
					Korisnik kor = new Korisnik();
					kor.setId(loggedInUserId);
					String datumRezervacije = sdf.format(now.getTime()).toString();
					String datumProdaje = null;
					
					if(povratniLetID.equals("null")) {
						povratniLetID = null;
						sedistePovratni = null;
					}
					boolean active = true;
					Rezervacija rez = new Rezervacija("",polazniLetID, povratniLetID, sedistePolazni, sedistePovratni,datumRezervacije,datumProdaje,kor,
							imePutnika,prezimePutnika,active);
					RezervacijaDAO.rezervacijaU3faze(rez);
					break;
				}
				case "delete": {
					String id = request.getParameter("id");
					RezervacijaDAO.deleteRezervacija(id);
					break;
				}
			}
			
			request.setAttribute("data", data);
			request.getRequestDispatcher("./SuccessServlet").forward(request, response);
		} catch (Exception ex) {
			ex.printStackTrace();
			request.getRequestDispatcher("./FailureServlet").forward(request, response);
		}
	}

}
