package app;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.KorisnikDAO;
import dao.LetDAO;
import dao.RezervacijaDAO;
import model.Korisnik;
import model.Let;
import model.Rezervacija;

public class LetoviKorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String loggedInUserId = (String) request.getSession().getAttribute("loggedInUserId");
		if (loggedInUserId == null) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		Korisnik loggedInUser = KorisnikDAO.get(loggedInUserId);
		if (loggedInUser == null) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		
		

		Map<String, Object> data = new LinkedHashMap<>();
		data.put("loggedInUserId", loggedInUserId);
		data.put("loggedInUser", loggedInUser);
		
		request.setAttribute("data", data);
		request.getRequestDispatcher("./SuccessServlet").forward(request, response);

	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String loggedInUserId = (String) request.getSession().getAttribute("loggedInUserId");
		if (loggedInUserId == null) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		Korisnik loggedInUser = KorisnikDAO.get(loggedInUserId);
		if (loggedInUser == null) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		
		
		Map<String, Object> data = new LinkedHashMap<>();
		

		try {
			String action = request.getParameter("action");
			String polazniLetID = null;
			String povratniLetID = null;
			switch (action) {
				case "faza3": {
						polazniLetID = request.getParameter("polazniLetID");
						povratniLetID = request.getParameter("povratniLetID");
						Let polazniLett = LetDAO.get(polazniLetID);
						Let povratniLett = LetDAO.get(povratniLetID);
						
						List<String> polazniSedistaSvi = new ArrayList<String>();
						List<String> povratniSedistaSvi = new ArrayList<String>();
						List<String> zauzetaPolazniSedistaSvi = new ArrayList<String>();
						List<String> zauzetaPovratniSedistaSvi = new ArrayList<String>();
						
						List<String> polazniSedista = new ArrayList<String>();
						List<String> povratniSedista = new ArrayList<String>();
						
						for (int i = 1; i <= polazniLett.getBrojSedista(); i++) {
							polazniSedista.add(String.valueOf(i));
							polazniSedistaSvi.add(String.valueOf(i));
						}
						
						if(!povratniLetID.equals("null")) {
							for (int i = 1; i <= povratniLett.getBrojSedista(); i++) {
								povratniSedista.add(String.valueOf(i));
								povratniSedistaSvi.add(String.valueOf(i));
							}
						}
						
						for(Rezervacija rezerv : RezervacijaDAO.getAll()) {
							if(povratniLetID == null) {
								if(polazniLetID.equals(rezerv.getPolazniLet())) {
									polazniSedista.remove(rezerv.getSedistePolazniLet());
									zauzetaPolazniSedistaSvi.add(rezerv.getSedistePolazniLet());
								}
								if(polazniLetID.equals(rezerv.getPovratniLet())) {
									polazniSedista.remove(rezerv.getSedistePovratniLet());
									zauzetaPolazniSedistaSvi.add(rezerv.getSedistePovratniLet());
								}
							}
							else {
								if(polazniLetID.equals(rezerv.getPolazniLet())) {
									polazniSedista.remove(rezerv.getSedistePolazniLet());
									zauzetaPolazniSedistaSvi.add(rezerv.getSedistePolazniLet());
								}
								if(polazniLetID.equals(rezerv.getPovratniLet())) {
									polazniSedista.remove(rezerv.getSedistePovratniLet());
									zauzetaPolazniSedistaSvi.add(rezerv.getSedistePovratniLet());
								}
								if(povratniLetID.equals(rezerv.getPolazniLet())) {
									povratniSedista.remove(rezerv.getSedistePolazniLet());
									zauzetaPovratniSedistaSvi.add(rezerv.getSedistePolazniLet());
								}
								if(povratniLetID.equals(rezerv.getPovratniLet())) {
									povratniSedista.remove(rezerv.getSedistePovratniLet());
									zauzetaPovratniSedistaSvi.add(rezerv.getSedistePovratniLet());
								}
							}
						}
						
						data.put("polazniLett", polazniLett);
						data.put("povratniLett", povratniLett);
						data.put("polazniSedista", polazniSedista);
						data.put("povratniSedista", povratniSedista);
						data.put("polazniSedistaSvi", polazniSedistaSvi);
						data.put("povratniSedistaSvi", povratniSedistaSvi);
						data.put("zauzetaPolazniSedistaSvi", zauzetaPolazniSedistaSvi);
						data.put("zauzetaPovratniSedistaSvi", zauzetaPovratniSedistaSvi);
					}
					
					break;
				}
			request.setAttribute("data", data);
			request.getRequestDispatcher("./SuccessServlet").forward(request, response);
		} catch (Exception ex) {
			ex.printStackTrace();
			request.getRequestDispatcher("./FailureServlet").forward(request, response);
		}
		
	}
}
