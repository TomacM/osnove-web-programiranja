package app;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.KorisnikDAO;
import model.Korisnik;
import model.Uloga;


public class PojedinacniKorisnikServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String loggedInUserId = (String) request.getSession().getAttribute("loggedInUserId");
		if (loggedInUserId == null) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		Korisnik loggedInUser = KorisnikDAO.get(loggedInUserId);

		String id = request.getParameter("id");
		
		if(loggedInUser.getUloga().name().equals("KORISNIK") && !(loggedInUser.getId().equals(id))) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}

		Korisnik korisnik = KorisnikDAO.get(id);
		

		List<Korisnik> listaKorisnika = KorisnikDAO.getAll();
		
		List<String> roles = new ArrayList<String>();
		
		for(Korisnik kor : listaKorisnika) {
			if(roles.contains(kor.getUloga().toString())) {
				continue;
			}
			roles.add(kor.getUloga().toString());
		}
		
		Map<String, Object> data = new LinkedHashMap<>();
		data.put("korisnik", korisnik);
		data.put("loggedInUserRole", loggedInUser.getUloga());
		data.put("roles", roles);
		
		request.setAttribute("data", data);
		request.getRequestDispatcher("./SuccessServlet").forward(request, response);	
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String loggedInUserId = (String) request.getSession().getAttribute("loggedInUserId");
		if (loggedInUserId == null) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		Korisnik loggedInUser = KorisnikDAO.get(loggedInUserId);
		if (loggedInUser == null) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		if (loggedInUser.getUloga() == null) {
			request.getRequestDispatcher("./UnauthorizedServlet").forward(request, response);
			return;
		}
		
		List<Korisnik> korisnici = KorisnikDAO.getAll();
		
		Map<String, Object> data = new LinkedHashMap<>();
		data.put("korisnici", korisnici);
		
		try {
			String action = request.getParameter("action");
			switch (action) {
				case "add": {
					break;
				}
				case "updatePutnikov": {		
					String korImePutnik = request.getParameter("korImePutnik");
					String lozinkaPutnik = request.getParameter("lozinkaPutnik");
					
					System.out.println("===========================");
					System.out.println(korImePutnik);
					System.out.println(lozinkaPutnik);
					System.out.println("===========================");
							
					
					String id = request.getParameter("id");
					Korisnik korisnik = KorisnikDAO.get(id);
					
					korisnik.setKorIme(korImePutnik);
					korisnik.setLozinka(lozinkaPutnik);
					
					if(id == null) {
						request.getRequestDispatcher("./FailureServlet").forward(request, response);
						return;
					}
					KorisnikDAO.updatePutnikov(korisnik);
					break;
				}
				case "updateAdminov": {
					String korIme = request.getParameter("korIme");
					String lozinka = request.getParameter("lozinka");
					String uloga = request.getParameter("uloga");	
					String blokiran = request.getParameter("blokiran");	
					
					String id = request.getParameter("id");
					Korisnik korisnik = KorisnikDAO.get(id);
					
					korisnik.setKorIme(korIme);
					korisnik.setLozinka(lozinka);
					korisnik.setUloga(Uloga.valueOf(uloga));
					korisnik.setBlokiran(Boolean.parseBoolean(blokiran));
					
					if(id == null) {
						request.getRequestDispatcher("./FailureServlet").forward(request, response);
						return;
					}
					KorisnikDAO.updateAdminov(korisnik);
					break;
				}
				case "delete":{
					String id = request.getParameter("id");
					if(!KorisnikDAO.deleteKorisnikZaProdate(id)) {
						KorisnikDAO.deleteKorisnikovihRezervacija(id);
						KorisnikDAO.deleteKorisnika(id);
					}
					else {
						KorisnikDAO.deleteKorisnikovihRezervacija(id);
					}
					
					break;
				}
			}
			
			request.getRequestDispatcher("./SuccessServlet").forward(request, response);
		} catch (Exception ex) {
			ex.printStackTrace();
			request.getRequestDispatcher("./FailureServlet").forward(request, response);
		}
	}

}
