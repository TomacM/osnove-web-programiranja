package app;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.LetDAO;
import model.Let;


public class LetoviServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text,html");
		
		
		
		
		List<Let> letovi = LetDAO.getAll();
		List<String> polazniAerodromi = new ArrayList<>();
		for(Let let : letovi) {
			if(polazniAerodromi.contains(let.getPolazniAerodrom())) {
				continue;
			}
			polazniAerodromi.add(let.getPolazniAerodrom());
		}
		List<String> dolazniAerodromi = new ArrayList<>();
		for(Let let : letovi) {
			if(dolazniAerodromi.contains(let.getDolazniAerodrom())) {
				continue;
			}
			dolazniAerodromi.add(let.getDolazniAerodrom());
		}
		
		List<Integer> brojeviLetova = new ArrayList<>();
		for(Let let : letovi) {
			int brojleta= Integer.parseInt(let.getBroj());
			brojeviLetova.add(brojleta);
		}

		
		String broj = request.getParameter("brojFilter");
		broj = (broj != null? broj: "");
		

		
		
		String lowDatumPolaskaDTFilter = request.getParameter("low");
		String highDatumPolaskaDTFilter = request.getParameter("high");
		
		String lowDatumDolaskaDTFilter = request.getParameter("lowD");
		String highDatumDolaskaDTFilter = request.getParameter("highD");
		String polazni = request.getParameter("polazni");
		String dolazni = request.getParameter("dolazni");
		
		String cena1 = request.getParameter("l");
		String cena2 = request.getParameter("h");
		

		
		List<Let> filteredDatum = LetDAO.getDatum(lowDatumPolaskaDTFilter, highDatumPolaskaDTFilter);
		List<Let> filteredDatumDolaska = LetDAO.getDatumDolaska(lowDatumDolaskaDTFilter, highDatumDolaskaDTFilter);
		List<Let> filteredPolazni = LetDAO.getPolazni(polazni);
		List<Let> filteredDolazni = LetDAO.getDolazni(dolazni);
		List<Let> filteredCena = LetDAO.getCena(cena1, cena2);
		
		
		
		System.out.println("=======================");
			System.out.println(polazni);
		System.out.println("=======================");
		


		
		Map<String, Object> data = new LinkedHashMap<>();
		data.put("polazniAerodromi", polazniAerodromi);
		data.put("dolazniAerodromi", dolazniAerodromi);
		data.put("brojeviLetova", brojeviLetova);
		data.put("filteredDatum", filteredDatum);
		data.put("filteredDatumDolaska", filteredDatumDolaska);
		data.put("filteredPolazni", filteredPolazni);
		data.put("filteredDolazni", filteredDolazni);
		data.put("filteredCena", filteredCena);
		
		
		List<Let> sviLetovi = LetDAO.getAll();
		
		if(broj!="") {
		List<Let> filteredBroj = LetDAO.getBroj(broj);
		data.put("filteredBroj", filteredBroj);
		}else {
		List<Let> filteredBroj = LetDAO.getAll();
		data.put("filteredBroj", filteredBroj);
		}
		
		data.put("sviLetovi", sviLetovi);
		
		

		request.setAttribute("data", data);
		request.getRequestDispatcher("./SuccessServlet").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
