package app;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.KorisnikDAO;
import dao.LetDAO;
import dao.RezervacijaDAO;
import model.Korisnik;
import model.Let;
import model.Rezervacija;


public class RezervacijaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String loggedInUserId = (String) request.getSession().getAttribute("loggedInUserId");
		if (loggedInUserId == null) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		Korisnik loggedInUser = KorisnikDAO.get(loggedInUserId);
		if (loggedInUser == null) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		if(loggedInUser.isBlokiran()==true) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		response.setContentType("text,html");
		String polazniLetID = request.getParameter("polazniLetID");
		
		
		String povratniLetID = request.getParameter("povratniLetID");
		
		Let polazniLett = LetDAO.get(polazniLetID);
		Let povratniLett = LetDAO.get(povratniLetID);
		
		List<Let> letovi = LetDAO.getAllZaRezKup();
		List<String> polazniAerodromi = new ArrayList<>();
		for(Let let : letovi) {
			if(polazniAerodromi.contains(let.getPolazniAerodrom())) {
				continue;
			}
			polazniAerodromi.add(let.getPolazniAerodrom());
		}
		List<String> dolazniAerodromi = new ArrayList<>();
		for(Let let : letovi) {
			if(dolazniAerodromi.contains(let.getDolazniAerodrom())) {
				continue;
			}
			dolazniAerodromi.add(let.getDolazniAerodrom());
		}
		
		List<Integer> brojeviLetova = new ArrayList<>();
		for(Let let : letovi) {
			int brojleta= Integer.parseInt(let.getBroj());
			brojeviLetova.add(brojleta);
		}

		
		String broj = request.getParameter("brojFilter");
		broj = (broj != null? broj: "");
		

		
		
		String lowDatumPolaskaDTFilter = request.getParameter("low");
		String highDatumPolaskaDTFilter = request.getParameter("high");
		
		String lowDatumDolaskaDTFilter = request.getParameter("lowD");
		String highDatumDolaskaDTFilter = request.getParameter("highD");
		String polazni = request.getParameter("polazni");
		String dolazni = request.getParameter("dolazni");
		
		String cena1 = request.getParameter("l");
		String cena2 = request.getParameter("h");
		
		String izabraniLetId = request.getParameter("id");
		

		List<Let> povratniLetovi = LetDAO.getAllPovratniZaRezKup(izabraniLetId);
		List<Let> filteredDatum = LetDAO.getDatumRez(lowDatumPolaskaDTFilter, highDatumPolaskaDTFilter);
		List<Let> filteredDatumDolaska = LetDAO.getDatumDolaskaRez(lowDatumDolaskaDTFilter, highDatumDolaskaDTFilter);
		List<Let> filteredPolazni = LetDAO.getPolazniRez(polazni);
		List<Let> filteredDolazni = LetDAO.getDolazniRez(dolazni);
		List<Let> filteredCena = LetDAO.getCenaRez(cena1, cena2);

		
		Map<String, Object> data = new LinkedHashMap<>();
		data.put("polazniAerodromi", polazniAerodromi);
		data.put("povratniLetovi", povratniLetovi);
		data.put("dolazniAerodromi", dolazniAerodromi);
		data.put("brojeviLetova", brojeviLetova);
		data.put("filteredDatum", filteredDatum);
		data.put("filteredDatumDolaska", filteredDatumDolaska);
		data.put("filteredPolazni", filteredPolazni);
		data.put("filteredDolazni", filteredDolazni);
		data.put("filteredCena", filteredCena);
		data.put("polazniLett", polazniLett);
		data.put("povratniLett", povratniLett);
		
		
		
		if(broj!="") {
		List<Let> filteredBroj = LetDAO.getBroj(broj);
		data.put("filteredBroj", filteredBroj);
		}else {
		List<Let> filteredBroj = LetDAO.getAll();
		data.put("filteredBroj", filteredBroj);
		}
		
		data.put("sviLetovi", letovi);
		
		

		request.setAttribute("data", data);
		request.getRequestDispatcher("./SuccessServlet").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String loggedInUserId = (String) request.getSession().getAttribute("loggedInUserId");
		if (loggedInUserId == null) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		
		Korisnik loggedInUser = KorisnikDAO.get(loggedInUserId);
		if (loggedInUser == null) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		if(loggedInUser.isBlokiran()==true) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		
		Map<String, Object> data = new LinkedHashMap<>();
		
		String polazniLetID = request.getParameter("polazniLetID");
		String povratniLetID = request.getParameter("povratniLetID");
		Let polazniLet = LetDAO.get(polazniLetID);
		Let povratniLet = LetDAO.get(povratniLetID);
		
		List<String> polazniSedistaSvi = new ArrayList<String>();
		List<String> povratniSedistaSvi = new ArrayList<String>();
		List<String> zauzetaPolazniSedistaSvi = new ArrayList<String>();
		List<String> zauzetaPovratniSedistaSvi = new ArrayList<String>();
		
		List<String> polazniSedista = new ArrayList<String>();
		List<String> povratniSedista = new ArrayList<String>();
		
		for (int i = 1; i <= polazniLet.getBrojSedista(); i++) {
			polazniSedista.add(String.valueOf(i));
			polazniSedistaSvi.add(String.valueOf(i));
		}
		
		if(!povratniLetID.equals("null")) {
			for (int i = 1; i <= povratniLet.getBrojSedista(); i++) {
				povratniSedista.add(String.valueOf(i));
				povratniSedistaSvi.add(String.valueOf(i));
			}
		}
		
		for(Rezervacija rezerv : RezervacijaDAO.getAll()) {
			if(povratniLetID == null) {
				if(polazniLetID.equals(rezerv.getPolazniLet())) {
					polazniSedista.remove(rezerv.getSedistePolazniLet());
					zauzetaPolazniSedistaSvi.add(rezerv.getSedistePolazniLet());
				}
				if(polazniLetID.equals(rezerv.getPovratniLet())) {
					polazniSedista.remove(rezerv.getSedistePovratniLet());
					zauzetaPolazniSedistaSvi.add(rezerv.getSedistePovratniLet());
				}
			}
			else {
				if(polazniLetID.equals(rezerv.getPolazniLet())) {
					polazniSedista.remove(rezerv.getSedistePolazniLet());
					zauzetaPolazniSedistaSvi.add(rezerv.getSedistePolazniLet());
				}
				if(polazniLetID.equals(rezerv.getPovratniLet())) {
					polazniSedista.remove(rezerv.getSedistePovratniLet());
					zauzetaPolazniSedistaSvi.add(rezerv.getSedistePovratniLet());
				}
				if(povratniLetID.equals(rezerv.getPolazniLet())) {
					povratniSedista.remove(rezerv.getSedistePolazniLet());
					zauzetaPovratniSedistaSvi.add(rezerv.getSedistePolazniLet());
				}
				if(povratniLetID.equals(rezerv.getPovratniLet())) {
					povratniSedista.remove(rezerv.getSedistePovratniLet());
					zauzetaPovratniSedistaSvi.add(rezerv.getSedistePovratniLet());
				}
			}
		}
		data.put("polazniLet", polazniLet);
		data.put("povratniLet", povratniLet);
		data.put("polazniSedista", polazniSedista);
		data.put("povratniSedista", povratniSedista);
		data.put("polazniSedistaSvi", polazniSedistaSvi);
		data.put("povratniSedistaSvi", povratniSedistaSvi);
		data.put("zauzetaPolazniSedistaSvi", zauzetaPolazniSedistaSvi);
		data.put("zauzetaPovratniSedistaSvi", zauzetaPovratniSedistaSvi);
		
	}

}
