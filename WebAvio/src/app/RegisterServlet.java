package app;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.KorisnikDAO;
import model.Korisnik;
import model.Uloga;



public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
			String userName = request.getParameter("userName");
			String password = request.getParameter("password");

			List<Korisnik> korisnici = KorisnikDAO.getAll();
			List<String> userNames = new ArrayList<String>();
			
			for(Korisnik kor : korisnici) {
				userNames.add(kor.getKorIme());
			}
			
			if(userNames.contains(userName)) {
				request.getRequestDispatcher("./FailureServlet").forward(request, response);
				return;
			}

			java.text.SimpleDateFormat sdf = 
			     new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Calendar now = Calendar.getInstance();
			
			Korisnik korisnik = new Korisnik("", userName, password, sdf.format(now.getTime()).toString(), Uloga.KORISNIK, false, true);
			KorisnikDAO.add(korisnik);


			Map<String, Object> data = new LinkedHashMap<>();

			request.setAttribute("data", data);
			request.getRequestDispatcher("./SuccessServlet").forward(request, response);
	}

}
