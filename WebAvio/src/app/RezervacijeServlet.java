package app;

import java.io.IOException;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.KorisnikDAO;
import dao.RezervacijaDAO;
import model.Korisnik;
import model.Rezervacija;



public class RezervacijeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String loggedInUserId = (String) request.getSession().getAttribute("loggedInUserId");
		if (loggedInUserId == null) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		Korisnik loggedInUser = KorisnikDAO.get(loggedInUserId);
		if (loggedInUser == null) {
			request.getRequestDispatcher("./LogoutServlet").forward(request, response);
			return;
		}
		
		String id = request.getParameter("id");
		
		List<Rezervacija> rezervacijePolazni = RezervacijaDAO.getAllRezervacijePolazni(id);
		List<Rezervacija> rezervacijePovratni = RezervacijaDAO.getAllRezervacijePovratni(id);
		List<Rezervacija> prodatePolazni = RezervacijaDAO.getAllProdatePolazni(id);
		List<Rezervacija> prodatePovratni = RezervacijaDAO.getAllProdatePovratni(id);
		List<Rezervacija> rezervacijeKorisnik = RezervacijaDAO.getAllRezervacijeKorisnik(id);
		List<Rezervacija> prodateKorisnik = RezervacijaDAO.getAllProdateKorisnik(id);
		
		
		Map<String, Object> data = new LinkedHashMap<>();
		data.put("rezervacijePolazni", rezervacijePolazni);
		data.put("rezervacijePovratni", rezervacijePovratni);
		data.put("prodatePolazni", prodatePolazni);
		data.put("prodatePovratni", prodatePovratni);
		data.put("rezervacijeKorisnik", rezervacijeKorisnik);
		data.put("prodateKorisnik", prodateKorisnik);
		data.put("loggedInUserRole", loggedInUser.getUloga());
		request.setAttribute("data", data);
		request.getRequestDispatcher("./SuccessServlet").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
