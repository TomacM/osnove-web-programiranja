package app;

import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.KorisnikDAO;

import model.Korisnik;



public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userName = request.getParameter("userName");
		String password = request.getParameter("password");

		Korisnik user = KorisnikDAO.get(userName, password);
		if (user == null) {
			request.getRequestDispatcher("./FailureServlet").forward(request, response);
			return;
		}

		request.getSession().setAttribute("loggedInUserId", user.getId());
		System.out.println("id: " + user.getId());

		request.getRequestDispatcher("./SuccessServlet").forward(request, response);
		return;
		
	}

}
