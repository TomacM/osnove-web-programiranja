package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import model.Let;

public class LetDAO {
	
	public static List<Let> getAll() {
		List<Let> letovi = new ArrayList<>();

		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		try {
			String query = "SELECT id, broj, datumPolaska, datumDolaska, polazniAerodrom, dolazniAerodrom, brojSedista, cena, active "
					+ "FROM let WHERE active = true";

			pstmt = conn.prepareStatement(query);

			rset = pstmt.executeQuery();
			
			while (rset.next()) {
				int index = 1;
				String ID = rset.getString(index++);
				String Broj = rset.getString(index++);
				String datumPolaska = rset.getString(index++);
				String datumDolaska = rset.getString(index++);
				String polazniAerodrom = rset.getString(index++);
				String dolazniAerodrom = rset.getString(index++);
				int brojSedista = rset.getInt(index++);
				double cena = rset.getDouble(index++);
				boolean active = rset.getBoolean(index++);


				Let let = new Let(ID,Broj,datumPolaska,datumDolaska,polazniAerodrom,dolazniAerodrom,
						brojSedista, cena, active);
				
				letovi.add(let);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return letovi;
	}
	public static Let get(String id) {
		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();
		

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT broj, datumPolaska, datumDolaska, polazniAerodrom, dolazniAerodrom, brojSedista, cena, active FROM "
					+ "let WHERE id = ?";

			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, id);
			System.out.println(pstmt);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				int index = 1;
				String broj = rset.getString(index++);
				String datumPolaska = rset.getString(index++);
				String datumDolaska = rset.getString(index++);
				String polazniAerodrom = rset.getString(index++);
				String dolazniAerodrom = rset.getString(index++);
				int brojSedista = rset.getInt(index++);
				double cena = rset.getDouble(index++);
				boolean active = rset.getBoolean(index++);

				return new Let(id,broj,datumPolaska,datumDolaska,polazniAerodrom,dolazniAerodrom,brojSedista,cena,active);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return null;
	}
	
	public static List<Let> getAllZaRezKup() {
		List<Let> letovi = new ArrayList<>();

		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();
		

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id, broj, datumPolaska, datumDolaska, polazniAerodrom, dolazniAerodrom, brojSedista, cena, active "
					+ "FROM let WHERE datumPolaska > now() and active = true";

			pstmt = conn.prepareStatement(query);

			rset = pstmt.executeQuery();
			
			while (rset.next()) {
				int index = 1;
				String ID = rset.getString(index++);
				String Broj = rset.getString(index++);
				String datumPolaska = rset.getString(index++);
				String datumDolaska = rset.getString(index++);
				String polazniAerodrom = rset.getString(index++);
				String dolazniAerodrom = rset.getString(index++);
				int brojSedista = rset.getInt(index++);
				double cena = rset.getDouble(index++);
				boolean active = rset.getBoolean(index++);


				Let let = new Let(ID,Broj,datumPolaska,datumDolaska,polazniAerodrom,dolazniAerodrom,
						brojSedista, cena, active);
				
				letovi.add(let);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return letovi;
	}
	
	public static List<Let> getAllPovratniZaRezKup(String polazniLetID) {
		List<Let> letovi = new ArrayList<>();

		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();
		

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT povratni.id, povratni.broj,povratni.datumPolaska, povratni.datumDolaska, povratni.polazniAerodrom, "
					+ "povratni.dolazniAerodrom, povratni.brojSedista, povratni.cena, povratni.active " + 
					"from let povratni, let polazni where polazni.id = ? and polazni.dolazniAerodrom = povratni.polazniAerodrom " + 
					"and polazni.polazniAerodrom = povratni.dolazniAerodrom and polazni.datumDolaska < povratni.datumPolaska " + 
					"and povratni.active = true";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, polazniLetID);
			rset = pstmt.executeQuery();
			
			while (rset.next()) {
				index = 1;
				String ID = rset.getString(index++);
				String Broj = rset.getString(index++);
				String datumPolaska = rset.getString(index++);
				String datumDolaska = rset.getString(index++);
				String polazniAerodrom = rset.getString(index++);
				String dolazniAerodrom = rset.getString(index++);
				int brojSedista = rset.getInt(index++);
				double cena = rset.getDouble(index++);
				boolean active = rset.getBoolean(index++);


				Let let = new Let(ID,Broj,datumPolaska,datumDolaska,polazniAerodrom,dolazniAerodrom,
						brojSedista, cena, active);
				
				letovi.add(let);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return letovi;
	}
	
	public static boolean add(Let let) {
		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();
		


		PreparedStatement pstmt = null;
		try {
			String query = "INSERT INTO let (broj, datumPolaska, datumDolaska, polazniAerodrom, dolazniAerodrom, brojSedista, cena, active) "
					+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, let.getBroj());
			pstmt.setString(index++, let.getDatumPolaska());
			pstmt.setString(index++, let.getDatumDolaska());
			pstmt.setString(index++, let.getPolazniAerodrom());
			pstmt.setString(index++, let.getDolazniAerodrom());
			pstmt.setInt(index++, let.getBrojSedista());
			pstmt.setDouble(index++, let.getCena());
			pstmt.setBoolean(index++, let.isActive());
			System.out.println(pstmt);

			return pstmt.executeUpdate() == 1;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return false;
	}
	
	public static boolean update(Let let) {
		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();
		


		PreparedStatement pstmt = null;
		try {
			String query = "UPDATE let SET datumPolaska = ?, datumDolaska = ?, polazniAerodrom = ?, dolazniAerodrom = ?, brojSedista = ?, cena = ? "
					+ "WHERE id = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, let.getDatumPolaska());
			pstmt.setString(index++, let.getDatumDolaska());
			pstmt.setString(index++, let.getPolazniAerodrom());
			pstmt.setString(index++, let.getDolazniAerodrom());
			pstmt.setInt(index++, let.getBrojSedista());
			pstmt.setDouble(index++, let.getCena());
			pstmt.setString(index++, let.getId());
			System.out.println(pstmt);

			return pstmt.executeUpdate() == 1;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return false;
	}
	
	public static boolean deleteRezervacijeLetova(String id) {
		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();
		


		
		PreparedStatement pstmt = null;
		try {
			String query = "DELETE FROM rezervacija WHERE polazniLetId = ? or povratniLetId = ? and datumRezervacije IS NOT NULL";

			int index = 1;
			pstmt = conn.prepareStatement(query);
			pstmt.setString(index++, id);
			pstmt.setString(index++, id);
			System.out.println(pstmt);

			return pstmt.executeUpdate() == 1;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return false;
	}
	
	public static boolean deleteProdateLetova(String id) {
		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();
		


		PreparedStatement pstmt = null;
		try {
			String query = "UPDATE let l, rezervacija r SET l.active = false WHERE l.id = ? and (r.polazniLetId = l.id or r.povratniLetId = l.id) and r.datumProdaje IS NOT NULL";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, id);
			System.out.println(pstmt);

			return pstmt.executeUpdate() == 1;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return false;
	}
	
	public static boolean deleteLetove(String id) {
		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();
		


		
		PreparedStatement pstmt = null;
		try {
			String query = "DELETE FROM let WHERE id = ?";

			int index = 1;
			pstmt = conn.prepareStatement(query);
			pstmt.setString(index++, id);
			System.out.println(pstmt);

			return pstmt.executeUpdate() == 1;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return false;
	}
	
	public static List<Let> getBrojRez(String broj) {
		List<Let> letovi = new ArrayList<>();

		
		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();
		

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT broj, datumPolaska, datumDolaska, polazniAerodrom, dolazniAerodrom, brojSedista, cena, active FROM "
					+ "let WHERE datumPolaska > now() and broj = ? and active=true";

			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, broj);
			System.out.println(pstmt);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				int index = 1;
				String id = rset.getString(index++);
				String datumPolaska = rset.getString(index++);
				String datumDolaska = rset.getString(index++);
				String polazniAerodrom = rset.getString(index++);
				String dolazniAerodrom = rset.getString(index++);
				int brojSedista = rset.getInt(index++);
				double cena = rset.getDouble(index++);
				boolean active = rset.getBoolean(index++);

				Let let= new Let(id,broj,datumPolaska,datumDolaska,polazniAerodrom,dolazniAerodrom,brojSedista,cena,active);
				
				letovi.add(let);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return letovi;
	}
	
	
	
	public static List<Let> getDatumRez(String datumPolaskaOd,String datumPolaskaDo) {
		List<Let> letovi = new ArrayList<>();

		
		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();
		

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id,broj, datumPolaska, datumDolaska, polazniAerodrom, dolazniAerodrom, brojSedista, cena, active FROM "
					+ "let WHERE datumPolaska > now() and datumPolaska >= ? and datumPolaska <= ? and active=true";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, datumPolaskaOd);
			pstmt.setString(index++, datumPolaskaDo);
			System.out.println(pstmt);


			rset = pstmt.executeQuery();

			while (rset.next()) {
				index = 1;
				String id = rset.getString(index++);
				String broj = rset.getString(index++);
				String datumPolaska = rset.getString(index++);
				String datumDolaska = rset.getString(index++);
				String polazniAerodrom = rset.getString(index++);
				String dolazniAerodrom = rset.getString(index++);
				int brojSedista = rset.getInt(index++);
				double cena = rset.getDouble(index++);
				boolean active = rset.getBoolean(index++);

				Let let= new Let(id,broj,datumPolaska,datumDolaska,polazniAerodrom,dolazniAerodrom,brojSedista,cena,active);
				
				letovi.add(let);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return letovi;
	}
	
	
	public static List<Let> getDatumDolaskaRez(String datumOd,String datumDo) {
		List<Let> letovi = new ArrayList<>();

		
		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();
		

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id,broj, datumPolaska, datumDolaska, polazniAerodrom, dolazniAerodrom, brojSedista, cena, active FROM "
					+ "let WHERE datumPolaska > now() and datumDolaska >= ? and datumDolaska <= ? and active=true";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, datumOd);
			pstmt.setString(index++, datumDo);
			System.out.println(pstmt);


			rset = pstmt.executeQuery();

			while (rset.next()) {
				index = 1;
				String id = rset.getString(index++);
				String broj = rset.getString(index++);
				String datumPolaska = rset.getString(index++);
				String datumDolaska = rset.getString(index++);
				String polazniAerodrom = rset.getString(index++);
				String dolazniAerodrom = rset.getString(index++);
				int brojSedista = rset.getInt(index++);
				double cena = rset.getDouble(index++);
				boolean active = rset.getBoolean(index++);

				Let let= new Let(id,broj,datumPolaska,datumDolaska,polazniAerodrom,dolazniAerodrom,brojSedista,cena,active);
				
				letovi.add(let);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return letovi;
	}
	
	public static List<Let> getPolazniRez(String aerodrom) {
		List<Let> letovi = new ArrayList<>();

		
		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();
		

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id,broj, datumPolaska, datumDolaska, polazniAerodrom, dolazniAerodrom, brojSedista, cena, active FROM "
					+ "let WHERE datumPolaska > now() and polazniAerodrom=? and active=true";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, aerodrom);
			System.out.println(pstmt);


			rset = pstmt.executeQuery();

			while (rset.next()) {
				index = 1;
				String id = rset.getString(index++);
				String broj = rset.getString(index++);
				String datumPolaska = rset.getString(index++);
				String datumDolaska = rset.getString(index++);
				String polazniAerodrom = rset.getString(index++);
				String dolazniAerodrom = rset.getString(index++);
				int brojSedista = rset.getInt(index++);
				double cena = rset.getDouble(index++);
				boolean active = rset.getBoolean(index++);

				Let let= new Let(id,broj,datumPolaska,datumDolaska,polazniAerodrom,dolazniAerodrom,brojSedista,cena,active);
				
				letovi.add(let);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return letovi;
	}
	
	public static List<Let> getDolazniRez(String aerodrom) {
		List<Let> letovi = new ArrayList<>();

		
		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();
		

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id,broj, datumPolaska, datumDolaska, polazniAerodrom, dolazniAerodrom, brojSedista, cena, active FROM "
					+ "let WHERE datumPolaska > now() and dolazniAerodrom=? and active=true";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, aerodrom);
			System.out.println(pstmt);


			rset = pstmt.executeQuery();

			while (rset.next()) {
				index = 1;
				String id = rset.getString(index++);
				String broj = rset.getString(index++);
				String datumPolaska = rset.getString(index++);
				String datumDolaska = rset.getString(index++);
				String polazniAerodrom = rset.getString(index++);
				String dolazniAerodrom = rset.getString(index++);
				int brojSedista = rset.getInt(index++);
				double cena = rset.getDouble(index++);
				boolean active = rset.getBoolean(index++);

				Let let= new Let(id,broj,datumPolaska,datumDolaska,polazniAerodrom,dolazniAerodrom,brojSedista,cena,active);
				
				letovi.add(let);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return letovi;
	}
	
	public static List<Let> getCenaRez(String cenaod,String cenado) {
		List<Let> letovi = new ArrayList<>();

		
		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();
		

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id,broj, datumPolaska, datumDolaska, polazniAerodrom, dolazniAerodrom, brojSedista, cena, active FROM "
					+ "let WHERE datumPolaska > now() and cena >= ? and cena <= ? and active=true";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, cenaod);
			pstmt.setString(index++, cenado);
			System.out.println(pstmt);


			rset = pstmt.executeQuery();

			while (rset.next()) {
				index = 1;
				String id = rset.getString(index++);
				String broj = rset.getString(index++);
				String datumPolaska = rset.getString(index++);
				String datumDolaska = rset.getString(index++);
				String polazniAerodrom = rset.getString(index++);
				String dolazniAerodrom = rset.getString(index++);
				int brojSedista = rset.getInt(index++);
				double cena = rset.getDouble(index++);
				boolean active = rset.getBoolean(index++);

				Let let= new Let(id,broj,datumPolaska,datumDolaska,polazniAerodrom,dolazniAerodrom,brojSedista,cena,active);
				
				letovi.add(let);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return letovi;
	}
	
	public static List<Let> getBroj(String broj) {
		List<Let> letovi = new ArrayList<>();

		
		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();
		

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT broj, datumPolaska, datumDolaska, polazniAerodrom, dolazniAerodrom, brojSedista, cena, active FROM "
					+ "let WHERE broj = ? and active=true";

			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, broj);
			System.out.println(pstmt);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				int index = 1;
				String id = rset.getString(index++);
				String datumPolaska = rset.getString(index++);
				String datumDolaska = rset.getString(index++);
				String polazniAerodrom = rset.getString(index++);
				String dolazniAerodrom = rset.getString(index++);
				int brojSedista = rset.getInt(index++);
				double cena = rset.getDouble(index++);
				boolean active = rset.getBoolean(index++);

				Let let= new Let(id,broj,datumPolaska,datumDolaska,polazniAerodrom,dolazniAerodrom,brojSedista,cena,active);
				
				letovi.add(let);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return letovi;
	}
	
	
	
	public static List<Let> getDatum(String datumPolaskaOd,String datumPolaskaDo) {
		List<Let> letovi = new ArrayList<>();

		
		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();
		

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id,broj, datumPolaska, datumDolaska, polazniAerodrom, dolazniAerodrom, brojSedista, cena, active FROM "
					+ "let WHERE datumPolaska >= ? and datumPolaska <= ? and active=true";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, datumPolaskaOd);
			pstmt.setString(index++, datumPolaskaDo);
			System.out.println(pstmt);


			rset = pstmt.executeQuery();

			while (rset.next()) {
				index = 1;
				String id = rset.getString(index++);
				String broj = rset.getString(index++);
				String datumPolaska = rset.getString(index++);
				String datumDolaska = rset.getString(index++);
				String polazniAerodrom = rset.getString(index++);
				String dolazniAerodrom = rset.getString(index++);
				int brojSedista = rset.getInt(index++);
				double cena = rset.getDouble(index++);
				boolean active = rset.getBoolean(index++);

				Let let= new Let(id,broj,datumPolaska,datumDolaska,polazniAerodrom,dolazniAerodrom,brojSedista,cena,active);
				
				letovi.add(let);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return letovi;
	}
	
	
	public static List<Let> getDatumDolaska(String datumOd,String datumDo) {
		List<Let> letovi = new ArrayList<>();

		
		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();
		

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id,broj, datumPolaska, datumDolaska, polazniAerodrom, dolazniAerodrom, brojSedista, cena, active FROM "
					+ "let WHERE datumDolaska >= ? and datumDolaska <= ? and active=true";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, datumOd);
			pstmt.setString(index++, datumDo);
			System.out.println(pstmt);


			rset = pstmt.executeQuery();

			while (rset.next()) {
				index = 1;
				String id = rset.getString(index++);
				String broj = rset.getString(index++);
				String datumPolaska = rset.getString(index++);
				String datumDolaska = rset.getString(index++);
				String polazniAerodrom = rset.getString(index++);
				String dolazniAerodrom = rset.getString(index++);
				int brojSedista = rset.getInt(index++);
				double cena = rset.getDouble(index++);
				boolean active = rset.getBoolean(index++);

				Let let= new Let(id,broj,datumPolaska,datumDolaska,polazniAerodrom,dolazniAerodrom,brojSedista,cena,active);
				
				letovi.add(let);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return letovi;
	}
	
	public static List<Let> getPolazni(String aerodrom) {
		List<Let> letovi = new ArrayList<>();

		
		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();
		

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id,broj, datumPolaska, datumDolaska, polazniAerodrom, dolazniAerodrom, brojSedista, cena, active FROM "
					+ "let WHERE polazniAerodrom=? and active=true";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, aerodrom);
			System.out.println(pstmt);


			rset = pstmt.executeQuery();

			while (rset.next()) {
				index = 1;
				String id = rset.getString(index++);
				String broj = rset.getString(index++);
				String datumPolaska = rset.getString(index++);
				String datumDolaska = rset.getString(index++);
				String polazniAerodrom = rset.getString(index++);
				String dolazniAerodrom = rset.getString(index++);
				int brojSedista = rset.getInt(index++);
				double cena = rset.getDouble(index++);
				boolean active = rset.getBoolean(index++);

				Let let= new Let(id,broj,datumPolaska,datumDolaska,polazniAerodrom,dolazniAerodrom,brojSedista,cena,active);
				
				letovi.add(let);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return letovi;
	}
	
	public static List<Let> getDolazni(String aerodrom) {
		List<Let> letovi = new ArrayList<>();

		
		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();
		

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id,broj, datumPolaska, datumDolaska, polazniAerodrom, dolazniAerodrom, brojSedista, cena, active FROM "
					+ "let WHERE dolazniAerodrom=? and active=true";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, aerodrom);
			System.out.println(pstmt);


			rset = pstmt.executeQuery();

			while (rset.next()) {
				index = 1;
				String id = rset.getString(index++);
				String broj = rset.getString(index++);
				String datumPolaska = rset.getString(index++);
				String datumDolaska = rset.getString(index++);
				String polazniAerodrom = rset.getString(index++);
				String dolazniAerodrom = rset.getString(index++);
				int brojSedista = rset.getInt(index++);
				double cena = rset.getDouble(index++);
				boolean active = rset.getBoolean(index++);

				Let let= new Let(id,broj,datumPolaska,datumDolaska,polazniAerodrom,dolazniAerodrom,brojSedista,cena,active);
				
				letovi.add(let);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return letovi;
	}
	
	public static List<Let> getCena(String cenaod,String cenado) {
		List<Let> letovi = new ArrayList<>();

		
		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();
		

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id,broj, datumPolaska, datumDolaska, polazniAerodrom, dolazniAerodrom, brojSedista, cena, active FROM "
					+ "let WHERE cena >= ? and cena <= ? and active=true";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, cenaod);
			pstmt.setString(index++, cenado);
			System.out.println(pstmt);


			rset = pstmt.executeQuery();

			while (rset.next()) {
				index = 1;
				String id = rset.getString(index++);
				String broj = rset.getString(index++);
				String datumPolaska = rset.getString(index++);
				String datumDolaska = rset.getString(index++);
				String polazniAerodrom = rset.getString(index++);
				String dolazniAerodrom = rset.getString(index++);
				int brojSedista = rset.getInt(index++);
				double cena = rset.getDouble(index++);
				boolean active = rset.getBoolean(index++);

				Let let= new Let(id,broj,datumPolaska,datumDolaska,polazniAerodrom,dolazniAerodrom,brojSedista,cena,active);
				
				letovi.add(let);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return letovi;
	}
	
	
	
	

	
	
	
	
	
	

}
