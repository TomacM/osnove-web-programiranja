package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import model.Korisnik;
import model.Uloga;

public class KorisnikDAO {

	public static Korisnik get(String userName, String password) {
		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id, datumRegistracije, uloga, blokiran, active FROM korisnik WHERE korisnickoIme = ? AND lozinka = ? and active = true";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, userName);
			pstmt.setString(index++, password);
			System.out.println(pstmt);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				index = 1;
				String id = rset.getString(index++);
				String datumRegistracije = rset.getString(index++);
				Uloga role = Uloga.valueOf(rset.getString(index++));
				boolean blokiran = rset.getBoolean(index++);
				boolean active = rset.getBoolean(index++);

				return new Korisnik(id, userName, password, datumRegistracije, role, blokiran, active);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
			try {
				rset.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
			try {
				conn.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
		}

		return null;
	}

	public static Korisnik get(String id) {

		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT korisnickoIme, lozinka, datumRegistracije, uloga, blokiran, active FROM korisnik WHERE id = ?";

			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, id);
			System.out.println(pstmt);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				int index = 1;
				String korisnickoIme = rset.getString(index++);
				String lozinka = rset.getString(index++);
				String datumRegistracije = rset.getString(index++);
				Uloga role = Uloga.valueOf(rset.getString(index++));
				boolean blokiran = rset.getBoolean(index++);
				boolean active = rset.getBoolean(index++);

				return new Korisnik(id, korisnickoIme, lozinka, datumRegistracije, role, blokiran, active);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
			try {
				rset.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
			try {
				conn.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
		}

		return null;
	}

	public static List<Korisnik> getAll() {
		List<Korisnik> korisnici = new ArrayList<>();

		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id, korisnickoIme, lozinka, datumRegistracije, uloga, blokiran, active "
					+ "FROM korisnik WHERE active = true";

			pstmt = conn.prepareStatement(query);

			rset = pstmt.executeQuery();

			while (rset.next()) {
				int index = 1;
				String id = rset.getString(index++);
				String korisnickoIme = rset.getString(index++);
				String lozinka = rset.getString(index++);
				String datumRegistracije = rset.getString(index++);
				Uloga role = Uloga.valueOf(rset.getString(index++));
				boolean blokiran = rset.getBoolean(index++);
				boolean active = rset.getBoolean(index++);

				Korisnik kor = new Korisnik(id, korisnickoIme, lozinka, datumRegistracije, role, blokiran, active);

				korisnici.add(kor);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
			try {
				rset.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
			try {
				conn.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
		}

		return korisnici;
	}

	public static List<Korisnik> getAll(String korIme, String uloga) {
		List<Korisnik> korisnici = new ArrayList<>();

		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "";
			if (uloga.equals("Svi")) {
				query = "SELECT id, korisnickoIme, lozinka, datumRegistracije, uloga, blokiran, active "
						+ "FROM korisnik WHERE korisnickoIme LIKE ? ";
			} else {
				query = "SELECT id, korisnickoIme, lozinka, datumRegistracije, uloga, blokiran, active "
						+ "FROM korisnik WHERE korisnickoIme LIKE ? AND uloga = ? ";
			}

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, "%" + korIme + "%");
			if (!uloga.equals("Svi")) {
				pstmt.setString(index++, uloga);
			}
			System.out.println(pstmt);
			rset = pstmt.executeQuery();

			while (rset.next()) {
				index = 1;
				String id = rset.getString(index++);
				String korisnickoIme = rset.getString(index++);
				String lozinka = rset.getString(index++);
				String datumRegistracije = rset.getString(index++);
				Uloga role = Uloga.valueOf(rset.getString(index++));
				boolean blokiran = rset.getBoolean(index++);
				boolean active = rset.getBoolean(index++);

				Korisnik kor = new Korisnik(id, korisnickoIme, lozinka, datumRegistracije, role, blokiran, active);

				korisnici.add(kor);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
			try {
				rset.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
			try {
				conn.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
		}

		return korisnici;
	}

	public static boolean add(Korisnik korisnik) {
		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();

		PreparedStatement pstmt = null;
		try {
			String query = "INSERT INTO korisnik (korisnickoIme, lozinka, datumRegistracije, uloga, blokiran, active) "
					+ "VALUES (?, ?, ?, ?, ?, ?)";

			pstmt = conn.prepareStatement(query);
			int index = 1;

			pstmt.setString(index++, korisnik.getKorIme());
			pstmt.setString(index++, korisnik.getLozinka());
			pstmt.setString(index++, korisnik.getDatumRegistracije());
			pstmt.setString(index++, korisnik.getUloga().toString());
			pstmt.setBoolean(index++, korisnik.isBlokiran());
			pstmt.setBoolean(index++, korisnik.isActive());
			System.out.println(pstmt);

			return pstmt.executeUpdate() == 1;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
			try {
				conn.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
		}

		return false;
	}

	public static boolean updatePutnikov(Korisnik kor) {
		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();

		PreparedStatement pstmt = null;
		try {
			String query = "UPDATE korisnik SET korisnickoIme = ?, lozinka = ? " + "WHERE id = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, kor.getKorIme());
			pstmt.setString(index++, kor.getLozinka());
			pstmt.setString(index++, kor.getId());
			System.out.println(pstmt);

			return pstmt.executeUpdate() == 1;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
			try {
				conn.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
		}

		return false;
	}

	public static boolean updateAdminov(Korisnik kor) {
		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();

		PreparedStatement pstmt = null;
		try {
			String query = "UPDATE korisnik SET korisnickoIme = ?, lozinka = ?, uloga = ?, blokiran = ? "
					+ "WHERE id = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, kor.getKorIme());
			pstmt.setString(index++, kor.getLozinka());
			pstmt.setString(index++, kor.getUloga().toString());
			pstmt.setBoolean(index++, kor.isBlokiran());
			pstmt.setString(index++, kor.getId());
			System.out.println(pstmt);

			return pstmt.executeUpdate() == 1;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
			try {
				conn.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
		}

		return false;
	}

	public static boolean deleteKorisnikovihRezervacija(String id) {
		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();

		PreparedStatement pstmt = null;
		try {
			String query = "DELETE FROM rezervacija WHERE korisnikId = ? and datumRezervacije IS NOT NULL ";

			int index = 1;
			pstmt = conn.prepareStatement(query);
			pstmt.setString(index++, id);
			System.out.println(pstmt);

			return pstmt.executeUpdate() == 1;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
			try {
				conn.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
		}

		return false;
	}

	public static boolean deleteKorisnikZaProdate(String id) {
		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();

		PreparedStatement pstmt = null;
		try {
			String query = "UPDATE korisnik k, rezervacija r SET k.active = false WHERE k.id = ? and r.korisnikId = k.id and r.datumProdaje IS NOT NULL;";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, id);
			System.out.println(pstmt);

			return pstmt.executeUpdate() == 1;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
			try {
				conn.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
		}

		return false;
	}

	public static boolean deleteKorisnika(String id) {
		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();

		PreparedStatement pstmt = null;
		try {
			String query = "DELETE FROM korisnik WHERE id = ?";

			int index = 1;
			pstmt = conn.prepareStatement(query);
			pstmt.setString(index++, id);
			System.out.println(pstmt);

			return pstmt.executeUpdate() == 1;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
			try {
				conn.close();
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
		}

		return false;
	}
	
	public static List<Korisnik> getKorImeAsc(String korImeFilter, String ulogaFilter) {
		List<Korisnik> korisnici = new ArrayList<>();

		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "";
			if(ulogaFilter.equals("Svi")) {
				query = "SELECT id, korisnickoIme, lozinka, datumRegistracije, uloga, blokiran, active "
						+ "FROM korisnik WHERE korisnickoIme LIKE ? order by korisnickoIme asc ";
			}
			else {
				query = "SELECT id, korisnickoIme, lozinka, datumRegistracije, uloga, blokiran, active "
						+ "FROM korisnik WHERE korisnickoIme LIKE ? AND uloga = ? order by korisnickoIme asc ";
			}
			

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, "%" + korImeFilter + "%");
			if(!ulogaFilter.equals("Svi")) {
				pstmt.setString(index++, ulogaFilter);
			}
			System.out.println(pstmt);
			rset = pstmt.executeQuery();
			
			while (rset.next()) {
				index = 1;
				String id = rset.getString(index++);
				String korisnickoIme = rset.getString(index++);
				String lozinka = rset.getString(index++);
				String datumRegistracije = rset.getString(index++);
				Uloga uloga = Uloga.valueOf(rset.getString(index++));
				boolean blokiran = rset.getBoolean(index++);
				boolean active = rset.getBoolean(index++);


				Korisnik kor = new Korisnik(id,korisnickoIme,lozinka,datumRegistracije,uloga,blokiran,active);
				
				korisnici.add(kor);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return korisnici;
	}
	
	public static List<Korisnik> getKorImeDesc(String korImeFilter, String ulogaFilter) {
		List<Korisnik> korisnici = new ArrayList<>();

		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "";
			if(ulogaFilter.equals("Svi")) {
				query = "SELECT id, korisnickoIme, lozinka, datumRegistracije, uloga, blokiran, active "
						+ "FROM korisnik WHERE korisnickoIme LIKE ? order by korisnickoIme desc ";
			}
			else {
				query = "SELECT id, korisnickoIme, lozinka, datumRegistracije, uloga, blokiran, active "
						+ "FROM korisnik WHERE korisnickoIme LIKE ? AND uloga = ? order by korisnickoIme desc ";
			}
			

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, "%" + korImeFilter + "%");
			if(!ulogaFilter.equals("Svi")) {
				pstmt.setString(index++, ulogaFilter);
			}
			System.out.println(pstmt);
			rset = pstmt.executeQuery();
			
			while (rset.next()) {
				index = 1;
				String id = rset.getString(index++);
				String korisnickoIme = rset.getString(index++);
				String lozinka = rset.getString(index++);
				String datumRegistracije = rset.getString(index++);
				Uloga uloga = Uloga.valueOf(rset.getString(index++));
				boolean blokiran = rset.getBoolean(index++);
				boolean active = rset.getBoolean(index++);


				Korisnik kor = new Korisnik(id,korisnickoIme,lozinka,datumRegistracije,uloga,blokiran,active);
				
				korisnici.add(kor);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return korisnici;
	}
	
	public static List<Korisnik> getUlogaAsc(String korImeFilter, String ulogaFilter) {
		List<Korisnik> korisnici = new ArrayList<>();

		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "";
			if(ulogaFilter.equals("Svi")) {
				query = "SELECT id, korisnickoIme, lozinka, datumRegistracije, uloga, blokiran, active "
						+ "FROM korisnik WHERE korisnickoIme LIKE ? order by uloga desc ";
			}
			else {
				query = "SELECT id, korisnickoIme, lozinka, datumRegistracije, uloga, blokiran, active "
						+ "FROM korisnik WHERE korisnickoIme LIKE ? AND uloga = ? order by uloga desc ";
			}
			

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, "%" + korImeFilter + "%");
			if(!ulogaFilter.equals("Svi")) {
				pstmt.setString(index++, ulogaFilter);
			}
			System.out.println(pstmt);
			rset = pstmt.executeQuery();
			
			while (rset.next()) {
				index = 1;
				String id = rset.getString(index++);
				String korisnickoIme = rset.getString(index++);
				String lozinka = rset.getString(index++);
				String datumRegistracije = rset.getString(index++);
				Uloga uloga = Uloga.valueOf(rset.getString(index++));
				boolean blokiran = rset.getBoolean(index++);
				boolean active = rset.getBoolean(index++);


				Korisnik kor = new Korisnik(id,korisnickoIme,lozinka,datumRegistracije,uloga,blokiran,active);
				
				korisnici.add(kor);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return korisnici;
	}
	
	public static List<Korisnik> getUlogaDesc(String korImeFilter, String ulogaFilter) {
		List<Korisnik> korisnici = new ArrayList<>();

		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "";
			if(ulogaFilter.equals("Svi")) {
				query = "SELECT id, korisnickoIme, lozinka, datumRegistracije, uloga, blokiran, active "
						+ "FROM korisnik WHERE korisnickoIme LIKE ? order by uloga asc ";
			}
			else {
				query = "SELECT id, korisnickoIme, lozinka, datumRegistracije, uloga, blokiran, active "
						+ "FROM korisnik WHERE korisnickoIme LIKE ? AND uloga = ? order by uloga asc ";
			}
			

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, "%" + korImeFilter + "%");
			if(!ulogaFilter.equals("Svi")) {
				pstmt.setString(index++, ulogaFilter);
			}
			System.out.println(pstmt);
			rset = pstmt.executeQuery();
			
			while (rset.next()) {
				index = 1;
				String id = rset.getString(index++);
				String korisnickoIme = rset.getString(index++);
				String lozinka = rset.getString(index++);
				String datumRegistracije = rset.getString(index++);
				Uloga uloga = Uloga.valueOf(rset.getString(index++));
				boolean blokiran = rset.getBoolean(index++);
				boolean active = rset.getBoolean(index++);


				Korisnik kor = new Korisnik(id,korisnickoIme,lozinka,datumRegistracije,uloga,blokiran,active);
				
				korisnici.add(kor);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return korisnici;
	}
}

