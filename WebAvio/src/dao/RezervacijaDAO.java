package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import model.Korisnik;
import model.Rezervacija;
import model.Uloga;

public class RezervacijaDAO {
	
	public static Rezervacija get(String id) {
		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "select r.polazniLetId, r.povratniLetId, r.sedistePolazniLet, r.sedistePovratniLet, r.datumRezervacije, r.datumProdaje, r.korisnikId, r.imePutnika, r.prezimePutnika, r.active, " + 
					"k.id, k.korisnickoIme, k.lozinka, k.datumRegistracije, k.uloga, k.blokiran, k.active " + 
					"from rezervacija r, korisnik k where k.id = r.korisnikId and r.id = ?";

			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, id);
			System.out.println(pstmt);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				int index = 1;
				String polazniLet = rset.getString(index++);
				String povratniLet = rset.getString(index++);
				String sedistePolazniLet = rset.getString(index++);
				String sedistePovratniLet = rset.getString(index++);
				String datumRezervacije = rset.getString(index++);
				String datumProdaje = rset.getString(index++);
				@SuppressWarnings("unused")
				String korisnikId = rset.getString(index++);
				String imePutnika = rset.getString(index++);
				String prezimePutnika = rset.getString(index++);
				boolean active = rset.getBoolean(index++);

				String korId = rset.getString(index++);
				String korKorIme = rset.getString(index++);
				String korLozinka = rset.getString(index++);
				String datumRegistracije = rset.getString(index++);
				Uloga uloga = Uloga.valueOf(rset.getString(index++));
				boolean korBlokiran = rset.getBoolean(index++);
				boolean korActive = rset.getBoolean(index++);
				
				
				Korisnik korisnik = new Korisnik(korId, korKorIme, korLozinka, datumRegistracije, uloga, korBlokiran, korActive);
				
				return new Rezervacija(id, polazniLet, povratniLet, sedistePolazniLet, sedistePovratniLet, datumRezervacije, datumProdaje, korisnik, imePutnika, prezimePutnika, active );
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return null;
	}
	
	public static List<Rezervacija> getAll() {
		List<Rezervacija> rezervacije = new ArrayList<>();

		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "select r.id, r.polazniLetId, r.povratniLetId, r.sedistePolazniLet, r.sedistePovratniLet, r.datumRezervacije, r.datumProdaje, r.korisnikId, r.imePutnika, r.prezimePutnika, r.active " +
					"from rezervacija r";

			pstmt = conn.prepareStatement(query);
			System.out.println(pstmt);

			rset = pstmt.executeQuery();
			
			while (rset.next()) {
				int index = 1;
				String id = rset.getString(index++);
				String polazniLet = rset.getString(index++);
				String povratniLet = rset.getString(index++);
				String sedistePolazniLet = rset.getString(index++);
				String sedistePovratniLet = rset.getString(index++);
				String datumRezervacije = rset.getString(index++);
				String datumProdaje = rset.getString(index++);
				String korisnikId = rset.getString(index++);
				String imePutnika = rset.getString(index++);
				String prezimePutnika = rset.getString(index++);
				boolean active = rset.getBoolean(index++);
				
				
				Korisnik kor = new Korisnik();
				kor.setId(korisnikId);
				
				Rezervacija rez = new Rezervacija(id, polazniLet, povratniLet, sedistePolazniLet, sedistePovratniLet, 
						datumRezervacije, datumProdaje, kor, imePutnika, prezimePutnika, active);
				
				rezervacije.add(rez);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return rezervacije;
	}
	
	public static List<Rezervacija> getAllRezervacijePolazni(String id1) {
		List<Rezervacija> rezervacije = new ArrayList<>();

		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "select r.id, r.polazniLetId, r.povratniLetId, r.sedistePolazniLet, r.sedistePovratniLet, r.datumRezervacije, r.datumProdaje, k.korisnickoIme, r.imePutnika, r.prezimePutnika, r.active, "
					+ "k.id, k.lozinka, k.datumRegistracije, k.uloga, k.blokiran, k.active " + 
					"from rezervacija r, korisnik k where k.id=r.korisnikId and r.datumRezervacije IS NOT NULL and  r.polazniLetId = ?";

			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, id1);
			System.out.println(pstmt);

			rset = pstmt.executeQuery();
			
			while (rset.next()) {
				int index = 1;
				String id = rset.getString(index++);
				String polazniLet = rset.getString(index++);
				String povratniLet = rset.getString(index++);
				String sedistePolazniLet = rset.getString(index++);
				String sedistePovratniLet = rset.getString(index++);
				String datumRezervacije = rset.getString(index++);
				String datumProdaje = rset.getString(index++);
				String korisnickoIme = rset.getString(index++);
				String imePutnika = rset.getString(index++);
				String prezimePutnika = rset.getString(index++);
				boolean active = rset.getBoolean(index++);
				
				String korId = rset.getString(index++);
				String korLozinka = rset.getString(index++);
				String korDatumReg = rset.getString(index++);
				Uloga uloga = Uloga.valueOf(rset.getString(index++));
				boolean blokiran = rset.getBoolean(index++);
				boolean active1 = rset.getBoolean(index++);
				
				Korisnik kor = new Korisnik(korId, korisnickoIme, korLozinka, korDatumReg, uloga, blokiran, active1);
				
				Rezervacija rez = new Rezervacija(id, polazniLet, povratniLet, sedistePolazniLet, sedistePovratniLet, 
						datumRezervacije, datumProdaje, kor, imePutnika, prezimePutnika, active);
				
				rezervacije.add(rez);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return rezervacije;
	}
	
	public static List<Rezervacija> getAllRezervacijePovratni(String id1) {
		List<Rezervacija> rezervacije = new ArrayList<>();

		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "select r.id, r.polazniLetId, r.povratniLetId, r.sedistePolazniLet, r.sedistePovratniLet, r.datumRezervacije, r.datumProdaje, k.korisnickoIme, r.imePutnika, r.prezimePutnika, r.active, "
					+ "k.id, k.lozinka, k.datumRegistracije, k.uloga, k.blokiran, k.active " + 
					"from rezervacija r, korisnik k where k.id=r.korisnikId and r.datumRezervacije IS NOT NULL and  r.povratniLetId = ?";

			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, id1);
			System.out.println(pstmt);

			rset = pstmt.executeQuery();
			
			while (rset.next()) {
				int index = 1;
				String id = rset.getString(index++);
				String polazniLet = rset.getString(index++);
				String povratniLet = rset.getString(index++);
				String sedistePolazniLet = rset.getString(index++);
				String sedistePovratniLet = rset.getString(index++);
				String datumRezervacije = rset.getString(index++);
				String datumProdaje = rset.getString(index++);
				String korisnickoIme = rset.getString(index++);
				String imePutnika = rset.getString(index++);
				String prezimePutnika = rset.getString(index++);
				boolean active = rset.getBoolean(index++);
				
				String korId = rset.getString(index++);
				String korLozinka = rset.getString(index++);
				String korDatumReg = rset.getString(index++);
				Uloga uloga = Uloga.valueOf(rset.getString(index++));
				boolean blokiran = rset.getBoolean(index++);
				boolean active1 = rset.getBoolean(index++);
				
				Korisnik kor = new Korisnik(korId, korisnickoIme, korLozinka, korDatumReg, uloga, blokiran, active1);
				
				Rezervacija rez = new Rezervacija(id, polazniLet, povratniLet, sedistePolazniLet, sedistePovratniLet, 
						datumRezervacije, datumProdaje, kor, imePutnika, prezimePutnika, active);
				
				rezervacije.add(rez);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return rezervacije;
	}
	
	public static List<Rezervacija> getAllProdatePolazni(String id1) {
		List<Rezervacija> prodate = new ArrayList<>();

		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "select r.id, r.polazniLetId, r.povratniLetId, r.sedistePolazniLet, r.sedistePovratniLet, r.datumRezervacije, r.datumProdaje, k.korisnickoIme, r.imePutnika, r.prezimePutnika, r.active, "
					+ "k.id, k.lozinka, k.datumRegistracije, k.uloga, k.blokiran, k.active " + 
					"from rezervacija r, korisnik k where k.id=r.korisnikId and r.datumProdaje IS NOT NULL and  r.polazniLetId = ?";

			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, id1);
			System.out.println(pstmt);

			rset = pstmt.executeQuery();
			
			while (rset.next()) {
				int index = 1;
				String id = rset.getString(index++);
				String polazniLet = rset.getString(index++);
				String povratniLet = rset.getString(index++);
				String sedistePolazniLet = rset.getString(index++);
				String sedistePovratniLet = rset.getString(index++);
				String datumRezervacije = rset.getString(index++);
				String datumProdaje = rset.getString(index++);
				String korisnickoIme = rset.getString(index++);
				String imePutnika = rset.getString(index++);
				String prezimePutnika = rset.getString(index++);
				boolean active = rset.getBoolean(index++);
				
				String korId = rset.getString(index++);
				String korLozinka = rset.getString(index++);
				String korDatumReg = rset.getString(index++);
				Uloga uloga = Uloga.valueOf(rset.getString(index++));
				boolean blokiran = rset.getBoolean(index++);
				boolean active1 = rset.getBoolean(index++);
				
				Korisnik kor = new Korisnik(korId, korisnickoIme, korLozinka, korDatumReg, uloga, blokiran, active1);
				
				Rezervacija rez = new Rezervacija(id, polazniLet, povratniLet, sedistePolazniLet, sedistePovratniLet, 
						datumRezervacije, datumProdaje, kor, imePutnika, prezimePutnika, active);
				
				prodate.add(rez);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return prodate;
	}
	
	public static List<Rezervacija> getAllProdatePovratni(String id1) {
		List<Rezervacija> prodate = new ArrayList<>();

		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "select r.id, r.polazniLetId, r.povratniLetId, r.sedistePolazniLet, r.sedistePovratniLet, r.datumRezervacije, r.datumProdaje, k.korisnickoIme, r.imePutnika, r.prezimePutnika, r.active, "
					+ "k.id, k.lozinka, k.datumRegistracije, k.uloga, k.blokiran, k.active " + 
					"from rezervacija r, korisnik k where k.id=r.korisnikId and r.datumProdaje IS NOT NULL and  r.povratniLetId = ?";

			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, id1);
			System.out.println(pstmt);

			rset = pstmt.executeQuery();
			
			while (rset.next()) {
				int index = 1;
				String id = rset.getString(index++);
				String polazniLet = rset.getString(index++);
				String povratniLet = rset.getString(index++);
				String sedistePolazniLet = rset.getString(index++);
				String sedistePovratniLet = rset.getString(index++);
				String datumRezervacije = rset.getString(index++);
				String datumProdaje = rset.getString(index++);
				String korisnickoIme = rset.getString(index++);
				String imePutnika = rset.getString(index++);
				String prezimePutnika = rset.getString(index++);
				boolean active = rset.getBoolean(index++);
				
				String korId = rset.getString(index++);
				String korLozinka = rset.getString(index++);
				String korDatumReg = rset.getString(index++);
				Uloga uloga = Uloga.valueOf(rset.getString(index++));
				boolean blokiran = rset.getBoolean(index++);
				boolean active1 = rset.getBoolean(index++);
				
				Korisnik kor = new Korisnik(korId, korisnickoIme, korLozinka, korDatumReg, uloga, blokiran, active1);
				
				Rezervacija rez = new Rezervacija(id, polazniLet, povratniLet, sedistePolazniLet, sedistePovratniLet, 
						datumRezervacije, datumProdaje, kor, imePutnika, prezimePutnika, active);
				
				prodate.add(rez);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return prodate;
	}
	
	public static List<Rezervacija> getAllRezervacijeKorisnik(String id1) {
		List<Rezervacija> rezervacije = new ArrayList<>();

		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "select r.id, r.polazniLetId, r.povratniLetId, r.sedistePolazniLet, r.sedistePovratniLet, r.datumRezervacije, r.datumProdaje, k.korisnickoIme, r.imePutnika, r.prezimePutnika, r.active, "
					+ "k.id, k.lozinka, k.datumRegistracije, k.uloga, k.blokiran, k.active " + 
					"from rezervacija r, korisnik k where r.korisnikId = ? and r.korisnikId=k.id and r.datumRezervacije IS NOT NULL order by r.datumRezervacije desc";

			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, id1);
			System.out.println(pstmt);

			rset = pstmt.executeQuery();
			
			while (rset.next()) {
				int index = 1;
				String id = rset.getString(index++);
				String polazniLet = rset.getString(index++);
				String povratniLet = rset.getString(index++);
				String sedistePolazniLet = rset.getString(index++);
				String sedistePovratniLet = rset.getString(index++);
				String datumRezervacije = rset.getString(index++);
				String datumProdaje = rset.getString(index++);
				String korisnickoIme = rset.getString(index++);
				String imePutnika = rset.getString(index++);
				String prezimePutnika = rset.getString(index++);
				boolean active = rset.getBoolean(index++);
				
				String korId = rset.getString(index++);
				String korLozinka = rset.getString(index++);
				String korDatumReg = rset.getString(index++);
				Uloga uloga = Uloga.valueOf(rset.getString(index++));
				boolean blokiran = rset.getBoolean(index++);
				boolean active1 = rset.getBoolean(index++);
				
				Korisnik kor = new Korisnik(korId, korisnickoIme, korLozinka, korDatumReg, uloga, blokiran, active1);
				
				Rezervacija rez = new Rezervacija(id, polazniLet, povratniLet, sedistePolazniLet, sedistePovratniLet, 
						datumRezervacije, datumProdaje, kor, imePutnika, prezimePutnika, active);
				
				rezervacije.add(rez);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return rezervacije;
	}
	
	public static List<Rezervacija> getAllProdateKorisnik(String id1) {
		List<Rezervacija> prodate = new ArrayList<>();

		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "select r.id, r.polazniLetId, r.povratniLetId, r.sedistePolazniLet, r.sedistePovratniLet, r.datumRezervacije, r.datumProdaje, k.korisnickoIme, r.imePutnika, r.prezimePutnika, r.active, "
					+ "k.id, k.lozinka, k.datumRegistracije, k.uloga, k.blokiran, k.active " + 
					"from rezervacija r, korisnik k where r.korisnikId = ? and r.korisnikId=k.id and r.datumProdaje IS NOT NULL order by r.datumProdaje desc";

			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, id1);
			System.out.println(pstmt);

			rset = pstmt.executeQuery();
			
			while (rset.next()) {
				int index = 1;
				String id = rset.getString(index++);
				String polazniLet = rset.getString(index++);
				String povratniLet = rset.getString(index++);
				String sedistePolazniLet = rset.getString(index++);
				String sedistePovratniLet = rset.getString(index++);
				String datumRezervacije = rset.getString(index++);
				String datumProdaje = rset.getString(index++);
				String korisnickoIme = rset.getString(index++);
				String imePutnika = rset.getString(index++);
				String prezimePutnika = rset.getString(index++);
				boolean active = rset.getBoolean(index++);
				
				String korId = rset.getString(index++);
				String korLozinka = rset.getString(index++);
				String korDatumReg = rset.getString(index++);
				Uloga uloga = Uloga.valueOf(rset.getString(index++));
				boolean blokiran = rset.getBoolean(index++);
				boolean active1 = rset.getBoolean(index++);
				
				Korisnik kor = new Korisnik(korId, korisnickoIme, korLozinka, korDatumReg, uloga, blokiran, active1);
				
				Rezervacija rez = new Rezervacija(id, polazniLet, povratniLet, sedistePolazniLet, sedistePovratniLet, 
						datumRezervacije, datumProdaje, kor, imePutnika, prezimePutnika, active);
				
				prodate.add(rez);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return prodate;
	}
	
	public static boolean kupovinaKartePrekoRezervisane(String datumProdaje, String id) {

		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();
		PreparedStatement pstmt = null;
		try {
			String query = "UPDATE rezervacija SET datumRezervacije = NULL, datumProdaje = ? "
					+ "WHERE id = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, datumProdaje);
			pstmt.setString(index++, id);
			System.out.println(pstmt);

			return pstmt.executeUpdate() == 1;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return false;
	}
	
	public static boolean rezervacijaU3faze(Rezervacija rez) {

		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();
		PreparedStatement pstmt = null;
		try {
			String query = "INSERT INTO rezervacija (polazniLetId, povratniLetId, sedistePolazniLet, sedistePovratniLet, datumRezervacije, "
					+ "datumProdaje, korisnikId, imePutnika, prezimePutnika, active) "  
					+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, rez.getPolazniLet());
			if(rez.getPovratniLet() == null) {
				pstmt.setNull(index++, Types.INTEGER);	
			}
			else {
				pstmt.setString(index++, rez.getPovratniLet());
			}
			
			pstmt.setString(index++, rez.getSedistePolazniLet());
			if(rez.getSedistePovratniLet() == null) {
				pstmt.setString(index++, null);
			}
			else {
				pstmt.setString(index++, rez.getSedistePovratniLet());
			}
			
			pstmt.setString(index++, rez.getDatumRezervacije());
			if(rez.getDatumProdaje() == null) {
				pstmt.setString(index++, null);
			}
			else {
				pstmt.setString(index++, rez.getDatumProdaje());
			}
			pstmt.setString(index++, rez.getKorisnik().getId());
			pstmt.setString(index++, rez.getImePutnika());
			pstmt.setString(index++, rez.getPrezimePutnika());
			pstmt.setBoolean(index++, rez.isActive());
			System.out.println(pstmt);

			return pstmt.executeUpdate() == 1;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return false;
	}
	
	public static boolean kupovinaU3faze(Rezervacija kup) {

		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();
		PreparedStatement pstmt = null;
		try {
			String query = "INSERT INTO rezervacija (polazniLetId, povratniLetId, sedistePolazniLet, sedistePovratniLet, datumRezervacije, "
					+ "datumProdaje, korisnikId, imePutnika, prezimePutnika, active) "  
					+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, kup.getPolazniLet());
			if(kup.getPovratniLet() == null) {
				pstmt.setNull(index++, Types.INTEGER);	
			}
			else {
				pstmt.setString(index++, kup.getPovratniLet());
			}
			
			pstmt.setString(index++, kup.getSedistePolazniLet());
			if(kup.getSedistePovratniLet() == null) {
				pstmt.setString(index++, null);
			}
			else {
				pstmt.setString(index++, kup.getSedistePovratniLet());
			}
			
			if(kup.getDatumRezervacije() == null) {
				pstmt.setString(index++, null);
			}
			else {
				pstmt.setString(index++, kup.getDatumRezervacije());
			}
			pstmt.setString(index++, kup.getDatumProdaje());
			pstmt.setString(index++, kup.getKorisnik().getId());
			pstmt.setString(index++, kup.getImePutnika());
			pstmt.setString(index++, kup.getPrezimePutnika());
			pstmt.setBoolean(index++, kup.isActive());
			System.out.println(pstmt);

			return pstmt.executeUpdate() == 1;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return false;
	}
	
	public static boolean updateRezervacija(Rezervacija rez) {

		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();
		PreparedStatement pstmt = null;
		try {
			String query = "UPDATE rezervacija SET sedistePolazniLet = ?, sedistePovratniLet = ?, imePutnika = ?, prezimePutnika = ? "
					+ "WHERE id = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, rez.getSedistePolazniLet());
			pstmt.setString(index++, rez.getSedistePovratniLet());
			pstmt.setString(index++, rez.getImePutnika());
			pstmt.setString(index++, rez.getPrezimePutnika());
			pstmt.setString(index++, rez.getId());
			System.out.println(pstmt);

			return pstmt.executeUpdate() == 1;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return false;
	}
	
	public static boolean deleteRezervacija(String id) {

		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();
		
		PreparedStatement pstmt = null;
		try {
			String query = "DELETE FROM rezervacija WHERE id = ?";

			int index = 1;
			pstmt = conn.prepareStatement(query);
			pstmt.setString(index++, id);
			System.out.println(pstmt);

			return pstmt.executeUpdate() == 1;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return false;
	}
	
	public static List<Rezervacija> getDatumProdajeDesc(String id1) {
		List<Rezervacija> rezervacije = new ArrayList<>();


		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {


			String query = "select r.id, r.polazniLetId, r.povratniLetId, r.sedistePolazniLet, r.sedistePovratniLet, r.datumRezervacije, r.datumProdaje, k.korisnickoIme, r.imePutnika, r.prezimePutnika, r.active, "
					+ "k.id, k.lozinka, k.datumRegistracije, k.uloga, k.blokiran, k.active " + 
					"from rezervacija r, korisnik k where k.id=r.korisnikId and r.datumProdaje IS NOT NULL and (r.polazniLetId = ? OR r.povratniLetId = ?) order by r.datumProdaje desc";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, id1);
			pstmt.setString(2, id1);
			System.out.println(pstmt);

			rset = pstmt.executeQuery();
			
			while (rset.next()) {
				int index = 1;
				String id = rset.getString(index++);
				String polazniLet = rset.getString(index++);
				String povratniLet = rset.getString(index++);
				String sedistePolazniLet = rset.getString(index++);
				String sedistePovratniLet = rset.getString(index++);
				String datumRezervacije = rset.getString(index++);
				String datumProdaje = rset.getString(index++);
				String korisnickoIme = rset.getString(index++);
				String imePutnika = rset.getString(index++);
				String prezimePutnika = rset.getString(index++);
				boolean active = rset.getBoolean(index++);
				
				String korId = rset.getString(index++);
				String korLozinka = rset.getString(index++);
				String korDatumReg = rset.getString(index++);
				Uloga uloga = Uloga.valueOf(rset.getString(index++));
				boolean blokiran = rset.getBoolean(index++);
				boolean active1 = rset.getBoolean(index++);
				
				Korisnik kor = new Korisnik(korId, korisnickoIme, korLozinka, korDatumReg, uloga, blokiran, active1);
				
				Rezervacija rez = new Rezervacija(id, polazniLet, povratniLet, sedistePolazniLet, sedistePovratniLet, 
						datumRezervacije, datumProdaje, kor, imePutnika, prezimePutnika, active);

				
				
				rezervacije.add(rez);
				
				
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return rezervacije;
	}
}



