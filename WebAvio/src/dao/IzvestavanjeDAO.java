package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import model.Izvestavanje;

public class IzvestavanjeDAO {
	
	public static List<Izvestavanje> getAll(String datumMin, String datumMax) {
		List<Izvestavanje> izvestavanje = new ArrayList<>();

		ConnectionManager manager = new ConnectionManager();
		Connection conn = manager.getConn();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "select a.naziv, count(l.id) " + 
					"from aerodrom a inner join let l on a.naziv = l.dolazniAerodrom " + 
					"where l.datumPolaska > ? and l.datumPolaska < ? " + 
					"group by a.naziv order by a.naziv";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, datumMin);
			pstmt.setString(index++, datumMax);
			rset = pstmt.executeQuery();
			
			while (rset.next()) {
				index = 1;
				String naziv = rset.getString(index++);
				int brojLetova = rset.getInt(index++);

				Izvestavanje izv = new Izvestavanje(naziv,brojLetova,0,0);
	
				izvestavanje.add(izv);
			}
			PreparedStatement pstmt1 = null;
			ResultSet rset1 = null;
			try {
				query = "select l.dolazniAerodrom, count(r.datumProdaje), sum(l.cena) " + 
						"from let l inner join rezervacija r on l.id = r.polazniLetId or l.id = r.povratniLetId " + 
						"where r.datumProdaje > ? and r.datumProdaje < ? and l.datumPolaska > ? and l.datumPolaska < ? " + 
						"group by l.dolazniAerodrom order by l.dolazniAerodrom";
				
				pstmt1 = conn.prepareStatement(query);
				index = 1;
				pstmt1.setString(index++, datumMin);
				pstmt1.setString(index++, datumMax);
				pstmt1.setString(index++, datumMin);
				pstmt1.setString(index++, datumMax);
				rset1 = pstmt1.executeQuery();
				
				while (rset1.next()) {
					index = 1;
					String naziv = rset1.getString(index++);
					int brojProdatih = rset1.getInt(index++);
					double cena = rset1.getDouble(index++);
					for (Izvestavanje iz : izvestavanje) {
						if(naziv.contentEquals(iz.getNazivAero())) {
							iz.setBrojProdatihKarata(brojProdatih);
							iz.setCena(cena);
						}
					}
				}
			}catch(Exception ex) {
				ex.printStackTrace();
			}finally {
				try {pstmt1.close();} catch (Exception ex1) {ex1.printStackTrace();}
				try {rset1.close();} catch (Exception ex1) {ex1.printStackTrace();}
				try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
			try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}
		
		return izvestavanje;
	}

}
