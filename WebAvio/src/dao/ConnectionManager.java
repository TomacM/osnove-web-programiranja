package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {

	public Connection getConn() {

		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/webavio", "root", "root");
			return connect;

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}

		catch (SQLException e) {
			e.printStackTrace();
			return null;
		}

	}

}
