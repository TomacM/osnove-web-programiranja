$(document).ready(function() {
	history.pushState(null, null, location.href);
	 window.onpopstate = function () {
	      history.go(1);
		    };
		$.get('RezervacijaServlet', function(data){
			if (data.status == 'unauthenticated') {
				window.location.replace('Login.html');
				return;
			}
		});
		var id = window.location.search.slice(1).split('&')[0].split('=')[1];
		function getPovratniLetovi(){
			$.get('RezervacijaServlet', {'id': id}, function(data){
				var letoviTable = $('#letoviTable');
				letoviTable.find('tr:gt(1)').remove();
				var letovi = data.povratniLetovi;
				var idLet = window.location.search.slice(1).split('&')[0].split('=')[1];
				for (it in letovi) {
					letoviTable.append(
						// '<tbody>' +
							'<tr class="trtr">' + 
							'<td><a href="Faza3.html?id='+ idLet+'??'+letovi[it].id +'">' + letovi[it].broj + '</a></td>' + 
								'<td>' + letovi[it].datumPolaska + '</td>' + 
								'<td>' + letovi[it].datumDolaska + '</td>' + 
								'<td>' + letovi[it].polazniAerodrom + '</td>' + 
								'<td>' + letovi[it].dolazniAerodrom + '</td>' + 
								'<td>' + letovi[it].cena + '</td>' + 
							'</tr>' // +
						// '</tbody>'
					);
				}
			
				
		
			});
		}
		getPovratniLetovi()
		
		function pocetna(){
			$.get('LetoviKorServlet', function(data){
				
				if (data.status == 'unauthenticated') {
					window.location.replace('Login.html');
					return;
				}

				if (data.status == 'success') {
					var id = data.loggedInUserId;
					$('#faza1').append('<a href="Faza1.html">Prethodni korak</a>');
				}
			});
		}
		pocetna();

		 
			var id = window.location.search.slice(1).split('&')[0].split('=')[1];
			var a = document.getElementById('preskoci');
			a.setAttribute('href', 'Faza3.html?id='+ id +'??'+ 'null');
			var params = {
			'id': id
			};
		$.get('PojedinacniLetServlet',params, function(data){
		

			var lett = data.lett;
			var slobodnaSedista = data.slobodnaSedista;
			var d = new Date
			
			var godina=d.getFullYear()
			var mesec= d.getMonth()+1
			var dan = d.getDate()
			var mesec1 ="0"+mesec
			var sat = d.getHours()
			var minut = d.getMinutes()
			var sekund = d.getSeconds()
			
			var format =godina+"-"+mesec1+"-"+dan+" "+sat+":"+minut+":"+sekund
			console.log(format)
			console.log(format+"<"+lett.datumPolaska)
			
			if(format>lett.datumPolaska){
				alert("Greska")
				window.location.replace('Faza1.html');
				
			}
			
			$('#brojCell').text(String(lett.broj));
			$('#datumPolaskaCell').text(String(lett.datumPolaska));
			$('#datumDolaskaCell').text(String(lett.datumDolaska));
			$('#polazniAerodromCell').text(String(lett.polazniAerodrom));
			$('#dolazniAerodromCell').text(String(lett.dolazniAerodrom));
			$('#brojSedistaCell').text(String(lett.brojSedista));
			$('#brojSlobodnihSedistaCell').text(String(slobodnaSedista));
			$('#cenaCell').text(lett.cena);
		});
});