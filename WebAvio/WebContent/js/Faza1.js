		$(document).ready(function() {
			history.pushState(null, null, location.href);
		    window.onpopstate = function () {
		        history.go(1);
		    };
		$.get('RezervacijaServlet', function(data){
			if (data.status == 'unauthenticated') {
				window.location.replace('Login.html');
				return;
			}
		});
		
$('#lowDatumPolaskaTFilterInput').hide();
$('#highDatumPolaskaDFilterInput').hide();
$('#highDatumPolaskaTFilterInput').hide();
$('#polazniAeroFilterCB').hide();
$('#dolazniAeroFilterCB').hide();
$('#lowPriceFilterInput').hide();
$('#highPriceFilterInput').hide();
$('#brojFilterInput').hide();
$('#lowDatumPolaskaDFilterInput').hide();
$('#pretraziDatum1').hide();
$('#pretraziCenu').hide();
$('#pretraziDatum').hide();

$('#lowDatumDolaskaDFilterInput').hide();
$('#lowDatumDolaskaTFilterInput').hide();
$('#highDatumDolaskaDFilterInput').hide();
$('#highDatumDolaskaTFilterInput').hide();

	var letoviTable = $('#letoviTable');
	
	
	
	$.get('RezervacijaServlet', function(data){

		if (data.status == 'success') {
			
			letoviTable.find('tr:gt(1)').remove();
			
			var letovi = data.sviLetovi;			
			for (it in letovi) {
				letoviTable.append(
					// '<tbody>' +
						'<tr class="trtr">' + 
						'<td><a href="Faza2.html?id=' + letovi[it].id + '">' + letovi[it].broj + '</a></td>' + 
							'<td>' + letovi[it].datumPolaska + '</td>' + 
							'<td>' + letovi[it].datumDolaska + '</td>' + 
							'<td>' + letovi[it].polazniAerodrom + '</td>' + 
							'<td>' + letovi[it].dolazniAerodrom + '</td>' + 
							'<td>' + letovi[it].cena + '</td>' + 
						'</tr>' // +
					// '</tbody>'
				);
			}
		}
		$('#restart').on('click', function(event) {
			
			$.get('RezervacijaServlet', function(data){

				if (data.status == 'success') {
					
					letoviTable.find('tr:gt(1)').remove();
					
					var letovi = data.sviLetovi;			
					for (it in letovi) {
						letoviTable.append(
							// '<tbody>' +
								'<tr class="trtr">' + 
								'<td><a href="Faza2.html?id=' + letovi[it].id + '">' + letovi[it].broj + '</a></td>' + 
									'<td>' + letovi[it].datumPolaska + '</td>' + 
									'<td>' + letovi[it].datumDolaska + '</td>' + 
									'<td>' + letovi[it].polazniAerodrom + '</td>' + 
									'<td>' + letovi[it].dolazniAerodrom + '</td>' + 
									'<td>' + letovi[it].cena + '</td>' + 
								'</tr>' // +
							// '</tbody>'
						);
					}
				}
			});
		});
		
		$('#pretraziDatum').on('click', function(event) {
		    var lowD = document.getElementById("lowDatumDolaskaDFilterInput").value;
		    var lowT = document.getElementById("lowDatumDolaskaTFilterInput").value;
		    var higD = document.getElementById("highDatumDolaskaDFilterInput").value;
		    var highT= document.getElementById("highDatumDolaskaTFilterInput").value;
		    
		    if(lowD=='' || lowT=='' || higD=='' || highT=='' || (lowD+" "+lowT)>(higD+" "+highT)){
		    	alert("Moraju sva polja biti popunjena i od>do");
		    }else{
			    var lowD = document.getElementById("lowDatumDolaskaDFilterInput").value;
			    var lowT = document.getElementById("lowDatumDolaskaTFilterInput").value;
			    var higD = document.getElementById("highDatumDolaskaDFilterInput").value;
			    var highT= document.getElementById("highDatumDolaskaTFilterInput").value;
			    
				console.log('lowD:' + lowD);
				console.log('lowT: ' + lowT);
				console.log('higD: ' + higD);
				console.log('highT: ' + highT);
				
				var params = {
						'lowD':lowD+" "+lowT+":"+"00",
						'highD':higD+" "+highT+":"+"00", 
				}
				
				var letoviTable = $('#letoviTable');
				
				$.get('RezervacijaServlet',params, function(data){

					if (data.status == 'success') {
						
						letoviTable.find('tr:gt(1)').remove();
						
						var filteredDatumDolaska = data.filteredDatumDolaska;			
						for (it in filteredDatumDolaska) {
							letoviTable.append(
								// '<tbody>' +
									'<tr class="trtr">' + 
									'<td><a href="Faza2.html?id=' + filteredDatumDolaska[it].id + '">' + filteredDatumDolaska[it].broj + '</a></td>' + 
										'<td>' + filteredDatumDolaska[it].datumPolaska + '</td>' + 
										'<td>' + filteredDatumDolaska[it].datumDolaska + '</td>' + 
										'<td>' + filteredDatumDolaska[it].polazniAerodrom + '</td>' + 
										'<td>' + filteredDatumDolaska[it].dolazniAerodrom + '</td>' + 
										'<td>' + filteredDatumDolaska[it].cena + '</td>' + 
									'</tr>' // +
								// '</tbody>'
							);
						}
					}
				});
		    	
		    }
		    
			
		});
		function pocetna(){
			$.get('LetoviKorServlet', function(data){
				
				if (data.status == 'unauthenticated') {
					window.location.replace('Login.html');
					return;
				}

				if (data.status == 'success') {
					var id = data.loggedInUserId;
					$('#pocetna').append('<a href="LetoviKor.html">Pocetna</a>');
				}
			});
		}
		pocetna();
		 
		$('#pretraziDatum1').on('click', function(event) {
		    
		    var lowD = document.getElementById("lowDatumPolaskaDFilterInput").value;
		    var lowT = document.getElementById("lowDatumPolaskaTFilterInput").value;
		    var higD = document.getElementById("highDatumPolaskaDFilterInput").value;
		    var highT= document.getElementById("highDatumPolaskaTFilterInput").value;
		    
		    if(lowD=='' || lowT=='' || higD=='' || highT=='' || (lowD+" "+lowT)>(higD+" "+highT)){
		    	alert("Moraju sva polja biti popunjena i od>do");
		    }else{
			    var lowD = document.getElementById("lowDatumPolaskaDFilterInput").value;
			    var lowT = document.getElementById("lowDatumPolaskaTFilterInput").value;
			    var higD = document.getElementById("highDatumPolaskaDFilterInput").value;
			    var highT= document.getElementById("highDatumPolaskaTFilterInput").value;
			    
				console.log('lowD:' + lowD);
				console.log('lowT: ' + lowT);
				console.log('higD: ' + higD);
				console.log('highT: ' + highT);
				
				var params = {
						'low':lowD+" "+lowT+":"+"00",
						'high':higD+" "+highT+":"+"00", 
				}
				
				var letoviTable = $('#letoviTable');
				
				$.get('Rezervacija',params, function(data){

					if (data.status == 'success') {
						
						letoviTable.find('tr:gt(1)').remove();
						
						var filteredDatum = data.filteredDatum;			
						for (it in filteredDatum) {
							letoviTable.append(
								// '<tbody>' +
									'<tr class="trtr">' + 
									'<td><a href="Faza2.html?id=' + filteredDatum[it].id + '">' + filteredDatum[it].broj + '</a></td>' + 
										'<td>' + filteredDatum[it].datumPolaska + '</td>' + 
										'<td>' + filteredDatum[it].datumDolaska + '</td>' + 
										'<td>' + filteredDatum[it].polazniAerodrom + '</td>' + 
										'<td>' + filteredDatum[it].dolazniAerodrom + '</td>' + 
										'<td>' + filteredDatum[it].cena + '</td>' + 
									'</tr>' // +
								// '</tbody>'
							);
						}
					}
				});
		    	
		    }
		    
			
		});
		
		
		$('#pretraziCenu').on('click', function(event) {
		    var l = document.getElementById("lowPriceFilterInput").value;
		    var h = document.getElementById("highPriceFilterInput").value;

		    
		    if(l=='' || h=='' || Number(l)>Number(h)){
		    	alert("Moraju sva polja biti popunjena i od>do");
		    }else{
			    var l = document.getElementById("lowPriceFilterInput").value;
			    var h = document.getElementById("highPriceFilterInput").value;

			    
				console.log('lowD:' + l);
				console.log('lowT: ' + h);
	
				
				var params = {
						'l':l,
						'h':h, 
				}
				
				var letoviTable = $('#letoviTable');
				
				$.get('Rezervacija',params, function(data){

					if (data.status == 'success') {
						
						letoviTable.find('tr:gt(1)').remove();
						
						var filteredCena = data.filteredCena;			
						for (it in filteredCena) {
							letoviTable.append(
								// '<tbody>' +
									'<tr class="trtr">' + 
									'<td><a href="Faza2.html?id=' + filteredCena[it].id + '">' + filteredCena[it].broj + '</a></td>' + 
										'<td>' + filteredCena[it].datumPolaska + '</td>' + 
										'<td>' + filteredCena[it].datumDolaska + '</td>' + 
										'<td>' + filteredCena[it].polazniAerodrom + '</td>' + 
										'<td>' + filteredCena[it].dolazniAerodrom + '</td>' + 
										'<td>' + filteredCena[it].cena + '</td>' + 
									'</tr>' // +
								// '</tbody>'
							);
						}
					}
				});
		    	
		    }
		    
			
		});
		
		
		
		
		
		
		
		$('#opadajuceSortBroj').on('click', function(event) {
			
			$.get('RezervacijaServlet', function(data){
				
				if (data.status == 'unauthenticated') {
					window.location.replace('Login.html');
					return;
				}

				if (data.status == 'success') {
					
					
					letoviTable.find('tr:gt(1)').remove();
					
					let letovi = data.sviLetovi;
					
					let sortirani = letovi.sort( (a,b)=>{
						if( Number(a.broj) < Number(b.broj)) return 1;
						else if(Number(b.broj) < Number(a.broj)) return -1;
						else return 0;
					} );
					
					for (it in sortirani) {
						letoviTable.append(
							'<tr class="trtr">' + 
							'<td><a href="Faza2.html?id=' + sortirani[it].id + '">' + sortirani[it].broj + '</a></td>' + 
								'<td>' + sortirani[it].datumPolaska + '</td>' + 
								'<td>' + sortirani[it].datumDolaska + '</td>' + 
								'<td>' + sortirani[it].polazniAerodrom + '</td>' + 
								'<td>' + sortirani[it].dolazniAerodrom + '</td>' + 
								'<td>' + sortirani[it].cena + '</td>' + 
							'</tr>'
						)
					}
				}
			});
		});
		
		$('#rastuceSortBroj').on('click', function(event) {
			
			$.get('RezervacijaServlet', function(data){
				
				if (data.status == 'unauthenticated') {
					window.location.replace('Login.html');
					return;
				}

				if (data.status == 'success') {
					
					
					letoviTable.find('tr:gt(1)').remove();
					
					let letovi = data.sviLetovi;
					
					let sortirani = letovi.sort( (a,b)=>{
						if( Number(a.broj) > Number(b.broj)) return 1;
						else if(Number(b.broj) > Number(a.broj)) return -1;
						else return 0;
					} );
					
					for (it in sortirani) {
						letoviTable.append(
							'<tr class="trtr">' + 
							'<td><a href="Faza2.html?id=' + sortirani[it].id + '">' + sortirani[it].broj + '</a></td>' + 
								'<td>' + sortirani[it].datumPolaska + '</td>' + 
								'<td>' + sortirani[it].datumDolaska + '</td>' + 
								'<td>' + sortirani[it].polazniAerodrom + '</td>' + 
								'<td>' + sortirani[it].dolazniAerodrom + '</td>' + 
								'<td>' + sortirani[it].cena + '</td>' + 
							'</tr>'
						)
					}
				}
			});
		});
		
		
		$('#rastuceSortDatumPolazak').on('click', function(event) {
			
			$.get('RezervacijaServlet', function(data){
				
				if (data.status == 'unauthenticated') {
					window.location.replace('Login.html');
					return;
				}

				if (data.status == 'success') {
					
					
					letoviTable.find('tr:gt(1)').remove();
					
					
					let letovi = data.sviLetovi;
					
					let sortirani = letovi.sort( (a,b)=>{
						if( a.datumPolaska > b.datumPolaska) return 1;
						else if(b.datumPolaska > a.datumPolaska) return -1;
						else return 0;
					} );
					
					
					
					for (it in sortirani) {
						letoviTable.append(
							'<tr class="trtr">' + 
							'<td><a href="Faza2.html?id=' + sortirani[it].id + '">' + sortirani[it].broj + '</a></td>' + 
								'<td>' + sortirani[it].datumPolaska + '</td>' + 
								'<td>' + sortirani[it].datumDolaska + '</td>' + 
								'<td>' + sortirani[it].polazniAerodrom + '</td>' + 
								'<td>' + sortirani[it].dolazniAerodrom + '</td>' + 
								'<td>' + sortirani[it].cena + '</td>' + 
							'</tr>'
						)
					}
				}
			});
		});
		
		$('#opadajuceSortDatumPolazak').on('click', function(event) {
			
			$.get('RezervacijaServlet', function(data){
				
				if (data.status == 'unauthenticated') {
					window.location.replace('Login.html');
					return;
				}

				if (data.status == 'success') {
					
					
					letoviTable.find('tr:gt(1)').remove();
					
					let letovi = data.sviLetovi;
					
					let sortirani = letovi.sort( (a,b)=>{
						if( a.datumPolaska < b.datumPolaska) return 1;
						else if(b.datumPolaska < a.datumPolaska) return -1;
						else return 0;
					} );
					
					
					for (it in sortirani) {
						letoviTable.append(
							'<tr class="trtr">' + 
							'<td><a href="Faza2.html?id=' + sortirani[it].id + '">' + sortirani[it].broj + '</a></td>' + 
								'<td>' + sortirani[it].datumPolaska + '</td>' + 
								'<td>' + sortirani[it].datumDolaska + '</td>' + 
								'<td>' + sortirani[it].polazniAerodrom + '</td>' + 
								'<td>' + sortirani[it].dolazniAerodrom + '</td>' + 
								'<td>' + sortirani[it].cena + '</td>' + 
							'</tr>'
						)
					}
				}
			});
		});
		
		
		$('#rastuceSortDatumDolazak').on('click', function(event) {
			
			$.get('RezervacijaServlet', function(data){
				
				if (data.status == 'unauthenticated') {
					window.location.replace('Login.html');
					return;
				}

				if (data.status == 'success') {
					
					
					letoviTable.find('tr:gt(1)').remove();
					
					let letovi = data.sviLetovi;
					
					let sortirani = letovi.sort( (a,b)=>{
						if( a.datumDolaska > b.datumDolaska) return 1;
						else if(b.datumDolaska > a.datumDolaska) return -1;
						else return 0;
					} );
					
					
					for (it in sortirani) {
						letoviTable.append(
							'<tr class="trtr">' + 
							'<td><a href="Faza2.html?id=' + sortirani[it].id + '">' + sortirani[it].broj + '</a></td>' + 
								'<td>' + sortirani[it].datumPolaska + '</td>' + 
								'<td>' + sortirani[it].datumDolaska + '</td>' + 
								'<td>' + sortirani[it].polazniAerodrom + '</td>' + 
								'<td>' + sortirani[it].dolazniAerodrom + '</td>' + 
								'<td>' + sortirani[it].cena + '</td>' + 
							'</tr>'
						)
					}
				}
			});
		});
		
		$('#opadajuceSortDatumDolazak').on('click', function(event) {
			
			$.get('RezervacijaServlet', function(data){
				
				if (data.status == 'unauthenticated') {
					window.location.replace('Login.html');
					return;
				}

				if (data.status == 'success') {
					
					
					letoviTable.find('tr:gt(1)').remove();
					
					let letovi = data.sviLetovi;
					
					let sortirani = letovi.sort( (a,b)=>{
						if( a.datumDolaska < b.datumDolaska) return 1;
						else if(b.datumDolaska < a.datumDolaska) return -1;
						else return 0;
					} );
					
					
					for (it in sortirani) {
						letoviTable.append(
							'<tr class="trtr">' + 
							'<td><a href="Faza2.html?id=' + sortirani[it].id + '">' + sortirani[it].broj + '</a></td>' + 
								'<td>' + sortirani[it].datumPolaska + '</td>' + 
								'<td>' + sortirani[it].datumDolaska + '</td>' + 
								'<td>' + sortirani[it].polazniAerodrom + '</td>' + 
								'<td>' + sortirani[it].dolazniAerodrom + '</td>' + 
								'<td>' + sortirani[it].cena + '</td>' + 
							'</tr>'
						)
					}
				}
			});
		});
		
		$('#rastuceSortPolazni').on('click', function(event) {
			
			$.get('RezervacijaServlet', function(data){
				
				if (data.status == 'unauthenticated') {
					window.location.replace('Login.html');
					return;
				}

				if (data.status == 'success') {
					
					
					letoviTable.find('tr:gt(1)').remove();
					
					let letovi = data.sviLetovi;
					
					let sortirani = letovi.sort( (a,b)=>{
						if( a.polazniAerodrom > b.polazniAerodrom) return 1;
						else if(b.polazniAerodrom > a.polazniAerodrom) return -1;
						else return 0;
					} );
					
					for (it in sortirani) {
						letoviTable.append(
							'<tr class="trtr">' + 
							'<td><a href="Faza2.html?id=' + sortirani[it].id + '">' + sortirani[it].broj + '</a></td>' + 
								'<td>' + sortirani[it].datumPolaska + '</td>' + 
								'<td>' + sortirani[it].datumDolaska + '</td>' + 
								'<td>' + sortirani[it].polazniAerodrom + '</td>' + 
								'<td>' + sortirani[it].dolazniAerodrom + '</td>' + 
								'<td>' + sortirani[it].cena + '</td>' + 
							'</tr>'
						)
					}
				}
			});
		});
		
		$('#opadajuceSortPolazni').on('click', function(event) {
			
			$.get('RezervacijaServlet', function(data){
				
				if (data.status == 'unauthenticated') {
					window.location.replace('Login.html');
					return;
				}

				if (data.status == 'success') {
					
					
					letoviTable.find('tr:gt(1)').remove();
					
					let letovi = data.sviLetovi;
					
					let sortirani = letovi.sort( (a,b)=>{
						if( a.polazniAerodrom < b.polazniAerodrom) return 1;
						else if(b.polazniAerodrom < a.polazniAerodrom) return -1;
						else return 0;
					} );
					
					
					for (it in sortirani) {
						letoviTable.append(
							'<tr class="trtr">' + 
							'<td><a href="Faza2.html?id=' + sortirani[it].id + '">' + sortirani[it].broj + '</a></td>' + 
								'<td>' + sortirani[it].datumPolaska + '</td>' + 
								'<td>' + sortirani[it].datumDolaska + '</td>' + 
								'<td>' + sortirani[it].polazniAerodrom + '</td>' + 
								'<td>' + sortirani[it].dolazniAerodrom + '</td>' + 
								'<td>' + sortirani[it].cena + '</td>' + 
							'</tr>'
						)
					}
				}
			});
		});
		
		
		
		
		$('#rastuceSortDolazni').on('click', function(event) {
			
			$.get('RezervacijaServlet', function(data){
				
				if (data.status == 'unauthenticated') {
					window.location.replace('Login.html');
					return;
				}

				if (data.status == 'success') {
					
					
					letoviTable.find('tr:gt(1)').remove();
					
					
					let letovi = data.sviLetovi;
					
					let sortirani = letovi.sort( (a,b)=>{
						if( a.dolazniAerodrom > b.dolazniAerodrom) return 1;
						else if(b.dolazniAerodrom > a.dolazniAerodrom) return -1;
						else return 0;
					} );
					
					for (it in sortirani) {
						letoviTable.append(
							'<tr class="trtr">' + 
							'<td><a href="Faza2.html?id=' + sortirani[it].id + '">' + sortirani[it].broj + '</a></td>' + 
								'<td>' + sortirani[it].datumPolaska + '</td>' + 
								'<td>' + sortirani[it].datumDolaska + '</td>' + 
								'<td>' + sortirani[it].polazniAerodrom + '</td>' + 
								'<td>' + sortirani[it].dolazniAerodrom + '</td>' + 
								'<td>' + sortirani[it].cena + '</td>' + 
							'</tr>'
						)
					}
				}
			});
		});
		
		$('#opadajuceSortDolazni').on('click', function(event) {
			
			$.get('RezervacijaServlet', function(data){
				
				if (data.status == 'unauthenticated') {
					window.location.replace('Login.html');
					return;
				}

				if (data.status == 'success') {
					
					
					letoviTable.find('tr:gt(1)').remove();
					
					let letovi = data.sviLetovi;
					
					let sortirani = letovi.sort( (a,b)=>{
						if( a.dolazniAerodrom < b.dolazniAerodrom) return 1;
						else if(b.dolazniAerodrom < a.dolazniAerodrom) return -1;
						else return 0;
					} );
					
					
					for (it in sortirani) {
						letoviTable.append(
							'<tr class="trtr">' + 
							'<td><a href="Faza2.html?id=' + sortirani[it].id + '">' + sortirani[it].broj + '</a></td>' + 
								'<td>' + sortirani[it].datumPolaska + '</td>' + 
								'<td>' + sortirani[it].datumDolaska + '</td>' + 
								'<td>' + sortirani[it].polazniAerodrom + '</td>' + 
								'<td>' + sortirani[it].dolazniAerodrom + '</td>' + 
								'<td>' + sortirani[it].cena + '</td>' + 
							'</tr>'
						)
					}
				}
			});
		});
		
		
		
	$('#rastuceSortCena').on('click', function(event) {
			
			$.get('RezervacijaServlet', function(data){
				
				if (data.status == 'unauthenticated') {
					window.location.replace('Login.html');
					return;
				}

				if (data.status == 'success') {
					
					
					letoviTable.find('tr:gt(1)').remove();
					
					let sortirani = letovi.sort( (a,b)=>{
						if( a.cena > b.cena) return 1;
						else if(b.cena > a.cena) return -1;
						else return 0;
					} );
					
					for (it in sortirani) {
						letoviTable.append(
							'<tr class="trtr">' + 
							'<td><a href="Faza2.html?id=' + sortirani[it].id + '">' + sortirani[it].broj + '</a></td>' + 
								'<td>' + sortirani[it].datumPolaska + '</td>' + 
								'<td>' + sortirani[it].datumDolaska + '</td>' + 
								'<td>' + sortirani[it].polazniAerodrom + '</td>' + 
								'<td>' + sortirani[it].dolazniAerodrom + '</td>' + 
								'<td>' + sortirani[it].cena + '</td>' + 
							'</tr>'
						)
					}
				}
			});
		});
		
		$('#opadajuceSortCena').on('click', function(event) {
			
			$.get('RezervacijaServlet', function(data){
				
				if (data.status == 'unauthenticated') {
					window.location.replace('Login.html');
					return;
				}

				if (data.status == 'success') {
					
					
					letoviTable.find('tr:gt(1)').remove();
					
					
					let sortirani = letovi.sort( (a,b)=>{
						if( a.cena < b.cena) return 1;
						else if(b.cena < a.cena) return -1;
						else return 0;
					} );
					
					for (it in sortirani) {
						letoviTable.append(
							'<tr class="trtr">' + 
							'<td><a href="Faza2.html?id=' + sortirani[it].id + '">' + sortirani[it].broj + '</a></td>' + 
								'<td>' + sortirani[it].datumPolaska + '</td>' + 
								'<td>' + sortirani[it].datumDolaska + '</td>' + 
								'<td>' + sortirani[it].polazniAerodrom + '</td>' + 
								'<td>' + sortirani[it].dolazniAerodrom + '</td>' + 
								'<td>' + sortirani[it].cena + '</td>' + 
							'</tr>'
						)
					}
				}
			});
		});
		
	});
	

	
	$('#brojFilterInput').keyup(function(event) {
	    if (event.keyCode === 13) {
	    
	    var brojFilterInput = $('#brojFilterInput');
		var brojFilter = brojFilterInput.val();
		console.log('brojFilter: ' + brojFilter);
		
		var params = {
				'brojFilter': brojFilter, 
		}

		var letoviTable = $('#letoviTable');
		
		$.get('RezervacijaServlet',params, function(data){

			if (data.status == 'success') {
				
				letoviTable.find('tr:gt(1)').remove();
				
				var letovi = data.filteredBroj;			
				for (it in letovi) {
					letoviTable.append(
						// '<tbody>' +
							'<tr class="trtr">' + 
							'<td><a href="Faza2.html?id=' + letovi[it].id + '">' + letovi[it].broj + '</a></td>' + 
								'<td>' + letovi[it].datumPolaska + '</td>' + 
								'<td>' + letovi[it].datumDolaska + '</td>' + 
								'<td>' + letovi[it].polazniAerodrom + '</td>' + 
								'<td>' + letovi[it].dolazniAerodrom + '</td>' + 
								'<td>' + letovi[it].cena + '</td>' + 
							'</tr>' // +
						// '</tbody>'
					);
				}
			}
		});
	    }
	});
	
	
	
	
	

	function cbPolazni(){
		polazniAeroFilterCB = document.getElementById('polazniAeroFilterCB');
		$.get('RezervacijaServlet', function(data){
			var polazniAerodromi = data.polazniAerodromi;
			var i;
			polazniAeroFilterCB.options[polazniAeroFilterCB.options.length] = new Option('Izaberi polazni aerodrom', 'None');	
			polazniAeroFilterCB.options[polazniAeroFilterCB.options.length] = new Option('Svi', 'Svi');	
			for (i = 0; i < polazniAerodromi.length; i++) { 
				polazniAeroFilterCB.options[polazniAeroFilterCB.options.length] = new Option(polazniAerodromi[i], polazniAerodromi[i]);	
			}	
			
		});
	}
	
	cbPolazni();
	
	function cbDolazni(){
		dolazniAeroFilterCB = document.getElementById('dolazniAeroFilterCB');
		$.get('RezervacijaServlet', function(data){
			var dolazniAerodromi = data.dolazniAerodromi;
			var i;
			dolazniAeroFilterCB.options[dolazniAeroFilterCB.options.length] = new Option('Izaberi dolazni aerodrom', 'None');	
			dolazniAeroFilterCB.options[dolazniAeroFilterCB.options.length] = new Option('Svi', 'Svi');	
			for (i = 0; i < dolazniAerodromi.length; i++) { 
				dolazniAeroFilterCB.options[dolazniAeroFilterCB.options.length] = new Option(dolazniAerodromi[i], dolazniAerodromi[i]);	
			}	
			
		});
	}
	cbDolazni();
	
	$('#izborPretrage').change(function(){
		  if($(this).val() == '1'){ // or this.value == 'volvo'
			  
				$('#brojFilterInput').show();
				$('#lowDatumPolaskaTFilterInput').hide();
				$('#highDatumPolaskaDFilterInput').hide();
				$('#highDatumPolaskaTFilterInput').hide();
				$('#polazniAeroFilterCB').hide();
				$('#dolazniAeroFilterCB').hide();
				$('#lowPriceFilterInput').hide();
				$('#highPriceFilterInput').hide();
				
				$('#rastuceSortBroj').hide();
				$('#opadajuceSortBroj').hide();
				$('#rastuceSortDatumPolazak').hide();
				$('#opadajuceSortDatumPolazak').hide();
				$('#rastuceSortDatumDolazak').hide();
				$('#opadajuceSortDatumDolazak').hide();
				$('#rastuceSortPolazni').hide();
				$('#opadajuceSortPolazni').hide();
				$('#rastuceSortDolazni').hide();
				$('#opadajuceSortDolazni').hide();
				$('#rastuceSortCena').hide();
				$('#opadajuceSortCena').hide();
				$('#pretraziCenu').hide();
				$('#lowDatumDolaskaDFilterInput').hide();
				$('#lowDatumDolaskaTFilterInput').hide();
				$('#highDatumDolaskaDFilterInput').hide();
				$('#highDatumDolaskaTFilterInput').hide();

				$('#pretraziDatum1').hide();
				$('#lowDatumPolaskaDFilterInput').hide();
				$('#pretraziDatum').hide();
		  }else if($(this).val() == '2'){ // or this.value == 'volvo'
				$('#highDatumPolaskaDFilterInput').show();
				$('#highDatumPolaskaTFilterInput').show();
				$('#lowDatumPolaskaTFilterInput').show();
				$('#polazniAeroFilterCB').hide();
				$('#dolazniAeroFilterCB').hide();
				$('#lowPriceFilterInput').hide();
				$('#highPriceFilterInput').hide();
				$('#brojFilterInput').hide();
				$('#lowDatumPolaskaDFilterInput').show();
				
				$('#rastuceSortBroj').hide();
				$('#opadajuceSortBroj').hide();
				$('#rastuceSortDatumPolazak').hide();
				$('#opadajuceSortDatumPolazak').hide();
				$('#rastuceSortDatumDolazak').hide();
				$('#opadajuceSortDatumDolazak').hide();
				$('#rastuceSortPolazni').hide();
				$('#opadajuceSortPolazni').hide();
				$('#rastuceSortDolazni').hide();
				$('#opadajuceSortDolazni').hide();
				$('#rastuceSortCena').hide();
				$('#pretraziCenu').hide();
				$('#opadajuceSortCena').hide();
				
				$('#lowDatumDolaskaDFilterInput').hide();
				$('#lowDatumDolaskaTFilterInput').hide();
				$('#highDatumDolaskaDFilterInput').hide();
				$('#highDatumDolaskaTFilterInput').hide();
				$('#pretraziDatum').hide();
				$('#pretraziDatum1').show();
		  }else if($(this).val() == '4'){ // or this.value == 'volvo'
				$('#polazniAeroFilterCB').show();
				$('#lowDatumPolaskaTFilterInput').hide();
				$('#highDatumPolaskaDFilterInput').hide();
				$('#highDatumPolaskaTFilterInput').hide();
				$('#dolazniAeroFilterCB').hide();
				$('#lowPriceFilterInput').hide();
				$('#highPriceFilterInput').hide();
				$('#brojFilterInput').hide();;
				$('#lowDatumPolaskaDFilterInput').hide();
				
				$('#rastuceSortBroj').hide();
				$('#opadajuceSortBroj').hide();
				$('#rastuceSortDatumPolazak').hide();
				$('#opadajuceSortDatumPolazak').hide();
				$('#rastuceSortDatumDolazak').hide();
				$('#opadajuceSortDatumDolazak').hide();
				$('#rastuceSortPolazni').hide();
				$('#opadajuceSortPolazni').hide();
				$('#rastuceSortDolazni').hide();
				$('#opadajuceSortDolazni').hide();
				$('#rastuceSortCena').hide();
				$('#opadajuceSortCena').hide();
				
				$('#pretraziDatum').hide();
				
				$('#lowDatumDolaskaDFilterInput').hide();
				$('#lowDatumDolaskaTFilterInput').hide();
				$('#highDatumDolaskaDFilterInput').hide();
				$('#pretraziCenu').hide();
				$('#highDatumDolaskaTFilterInput').hide();
				$('#pretraziDatum1').hide();
		  }else if($(this).val() == '5'){ // or this.value == 'volvo'
				$('#dolazniAeroFilterCB').show();
				$('#lowDatumPolaskaTFilterInput').hide();
				$('#highDatumPolaskaDFilterInput').hide();
				$('#highDatumPolaskaTFilterInput').hide();
				$('#polazniAeroFilterCB').hide();
				$('#lowPriceFilterInput').hide();
				$('#highPriceFilterInput').hide();
				$('#brojFilterInput').hide();
				$('#lowDatumPolaskaDFilterInput').hide();
				
				$('#rastuceSortBroj').hide();
				$('#opadajuceSortBroj').hide();
				$('#rastuceSortDatumPolazak').hide();
				$('#opadajuceSortDatumPolazak').hide();
				$('#rastuceSortDatumDolazak').hide();
				$('#opadajuceSortDatumDolazak').hide();
				$('#rastuceSortPolazni').hide();
				$('#opadajuceSortPolazni').hide();
				$('#rastuceSortDolazni').hide();
				$('#opadajuceSortDolazni').hide();
				$('#rastuceSortCena').hide();
				$('#opadajuceSortCena').hide();
				
				$('#lowDatumDolaskaDFilterInput').hide();
				$('#lowDatumDolaskaTFilterInput').hide();
				$('#highDatumDolaskaDFilterInput').hide();
				$('#highDatumDolaskaTFilterInput').hide();	
				$('#pretraziDatum1').hide();
				$('#pretraziDatum').hide();
				$('#pretraziCenu').hide();
				
				
				
				
				
		  }else if($(this).val() == '6'){ // or this.value == 'volvo'
				$('#lowPriceFilterInput').show();
				$('#highPriceFilterInput').show();
				$('#lowDatumPolaskaTFilterInput').hide();
				$('#highDatumPolaskaDFilterInput').hide();
				$('#highDatumPolaskaTFilterInput').hide();
				$('#polazniAeroFilterCB').hide();
				$('#dolazniAeroFilterCB').hide();
				$('#brojFilterInput').hide();
				$('#lowDatumPolaskaDFilterInput').hide();
				$('#pretraziDatum1').hide();
				
				$('#rastuceSortBroj').hide();
				$('#opadajuceSortBroj').hide();
				$('#rastuceSortDatumPolazak').hide();
				$('#opadajuceSortDatumPolazak').hide();
				$('#rastuceSortDatumDolazak').hide();
				$('#opadajuceSortDatumDolazak').hide();
				$('#rastuceSortPolazni').hide();
				$('#opadajuceSortPolazni').hide();
				$('#rastuceSortDolazni').hide();
				$('#opadajuceSortDolazni').hide();
				$('#rastuceSortCena').hide();
				$('#opadajuceSortCena').hide();
				
				$('#lowDatumDolaskaDFilterInput').hide();
				$('#lowDatumDolaskaTFilterInput').hide();
				$('#highDatumDolaskaDFilterInput').hide();
				$('#highDatumDolaskaTFilterInput').hide();

				$('#pretraziDatum1').hide();
				$('#pretraziDatum').hide();
				$('#pretraziCenu').show();
				
		  }
		  else if($(this).val() == '7'){ // or this.value == 'volvo'
				$('#lowDatumPolaskaTFilterInput').hide();
				$('#pretraziCenu').hide();
				$('#highDatumPolaskaDFilterInput').hide();
				$('#highDatumPolaskaTFilterInput').hide();
				$('#polazniAeroFilterCB').hide();
				$('#dolazniAeroFilterCB').hide();
				$('#lowPriceFilterInput').hide();
				$('#highPriceFilterInput').hide();
				$('#brojFilterInput').hide();
				$('#lowDatumPolaskaDFilterInput').hide();
				
				
				$('#lowDatumDolaskaDFilterInput').hide();
				$('#lowDatumDolaskaTFilterInput').hide();
				$('#highDatumDolaskaDFilterInput').hide();
				$('#highDatumDolaskaTFilterInput').hide();
				
				$('#rastuceSortBroj').show();
				$('#opadajuceSortBroj').show();
				$('#rastuceSortDatumPolazak').show();
				$('#opadajuceSortDatumPolazak').show();
				$('#rastuceSortDatumDolazak').show();
				$('#opadajuceSortDatumDolazak').show();
				$('#rastuceSortPolazni').show();
				$('#opadajuceSortPolazni').show();
				$('#rastuceSortDolazni').show();
				$('#opadajuceSortDolazni').show();
				$('#rastuceSortCena').show();
				$('#opadajuceSortCena').show();
				
				$('#pretraziDatum1').hide();
				$('#pretraziDatum').hide();
				
				location.reload(); 

		  }  else if($(this).val() == '3'){ // or this.value == 'volvo'
				$('#lowDatumPolaskaTFilterInput').hide();
				$('#highDatumPolaskaDFilterInput').hide();
				$('#highDatumPolaskaTFilterInput').hide();
				$('#polazniAeroFilterCB').hide();
				$('#dolazniAeroFilterCB').hide();
				$('#lowPriceFilterInput').hide();
				$('#highPriceFilterInput').hide();
				$('#brojFilterInput').hide();
				$('#lowDatumPolaskaDFilterInput').hide();
				
				$('#rastuceSortBroj').hide();
				$('#opadajuceSortBroj').hide();
				$('#rastuceSortDatumPolazak').hide();
				$('#opadajuceSortDatumPolazak').hide();
				$('#rastuceSortDatumDolazak').hide();
				$('#opadajuceSortDatumDolazak').hide();
				$('#rastuceSortPolazni').hide();
				$('#opadajuceSortPolazni').hide();
				$('#rastuceSortDolazni').hide();
				$('#opadajuceSortDolazni').hide();
				$('#rastuceSortCena').hide();
				$('#opadajuceSortCena').hide();
				
				$('#lowDatumDolaskaDFilterInput').show();
				$('#lowDatumDolaskaTFilterInput').show();
				$('#highDatumDolaskaDFilterInput').show();
				$('#highDatumDolaskaTFilterInput').show();
				$('#pretraziDatum1').hide();
				$('#pretraziDatum').show();
				$('#pretraziCenu').hide();
				

		  }
		});
	
	
	
	
	
});
