$(document).ready(function() {
	var userNameInput = $('#userNameInput');
	var passwordInput = $('#passwordInput');
	var repeatedPasswordInput = $('#repeatedPasswordInput');

	var messageParagraph = $('#messageParagraph');

	$('#registrationSubmit').on('click', function(event) {
		var userName = userNameInput.val();
		var password = passwordInput.val();
		var repeatedPassword = repeatedPasswordInput.val();
		console.log('userName: ' + userName);
		console.log('password: ' + password);
		console.log('repeated: ' + repeatedPassword);

		if(userName == ""){
			alert('Morate uneti korisnicko ime! ');
			event.preventDefault();
			return false;
		}
		else if(password == ""){
			alert('Morate uneti lozinku! ');
			event.preventDefault();
			return false;
		}
		else if (password != repeatedPassword) {
			alert('Lozinke se ne podudaraju! ');
			userNameInput.val('');
			passwordInput.val('');
			repeatedPasswordInput.val('');
			event.preventDefault();
			return false;
		}

		
		var params = {
			'userName': userName, 
			'password': password
		}
		$.post('RegisterServlet', params, function(data) {
			console.log(data);

			if (data.status == 'failure') {
				alert('Korisnicko ime vec postoji! ');
				userNameInput.val('');
				passwordInput.val('');
				repeatedPasswordInput.val('');
				return;
			}
			if (data.status == 'success') {
				window.location.replace('Letovi.html');
			}
		});

		event.preventDefault();
		return false;
	});
});