$(document).ready(function() {
	$.get('LetoviKorServlet',function(data){
		if (data.status == 'unauthenticated') {
			window.location.replace('Login.html');
			return;
		}
		if (data.loggedInUser.uloga != "ADMIN"){
			window.location.replace('LetoviKor.html');
			return;
		}
	});
	
	
	var korImeFilterInput = $('#korImeFilterInput');
	
	var ulogaCB;
	function getChecked(){
		ulogaCB = document.getElementById("ulogaCB").value;	
	}
	function pocetna(){
		$.get('LetoviKorServlet', function(data){
			
			if (data.status == 'unauthenticated') {
				window.location.replace('Login.html');
				return;
			}

			if (data.status == 'success') {
				var id = data.loggedInUserId;
				$('#pocetna').append('<a href="LetoviKor.html">Pocetna</a>');
			}
		});
	}
	pocetna();
	
	function cbUloga(){
		var ulogaCB = document.getElementById('ulogaCB');
		ulogaCB.options[ulogaCB.options.length] = new Option('Svi', 'Svi');
		$.get('UpravljanjeKorisnicimaServlet', function(data){
			var roles = data.roles;
			var i;	
			for (i = 0; i < roles.length; i++) { 
				ulogaCB.options[ulogaCB.options.length] = new Option(roles[i], roles[i]);	
			}	
			
			
		});
	}
	
	cbUloga();
	
	var korisniciTable = $('#korisniciTable');
	
	function getKorisnici(){
		getChecked();
		var korImeFilter = korImeFilterInput.val();
		var ulogaFilter = ulogaCB;
		
		
		
		var params = {
				'korImeFilter': korImeFilter,
				'ulogaFilter': ulogaFilter
		};
		
		$.get('UpravljanjeKorisnicimaServlet', params, function(data){
			
			if (data.status == 'unauthenticated') {
				window.location.replace('Login.html');
				return;
			}

			if (data.status == 'success') {
				
				korisniciTable.find('tr:gt(1)').remove();
				
				var filteredKorisnici = data.filteredKorisnici;
				for (it in filteredKorisnici) {
					korisniciTable.append(
						'<tr>' + 
						'<td><a href="PojedinacniKorisnik.html?id=' + filteredKorisnici[it].id + '">' + filteredKorisnici[it].korIme + '</a></td>' + 
							'<td>' + filteredKorisnici[it].datumRegistracije + '</td>' + 
							'<td>' + filteredKorisnici[it].uloga + '</td>' +
						'</tr>'
					)
				}
			}
		});
	}
	
	korImeFilterInput.on('keyup', function(event) {
		getKorisnici();
	
		event.preventDefault();
		return false;
	});
	
	$("#ulogaCB").on("change", getKorisnici);
	
	getKorisnici();
	
	$('#rastuceSortKorIme').on('click', function(event) {
		getChecked();
		var korImeFilter = korImeFilterInput.val();
		var ulogaFilter = ulogaCB;
		
		var params = {
				'korImeFilter': korImeFilter, 
				'ulogaFilter': ulogaFilter
			};
		$.get('UpravljanjeKorisnicimaServlet', params, function(data){
			
			if (data.status == 'unauthenticated') {
				window.location.replace('Login.html');
				return;
			}

			if (data.status == 'success') {
				
				
				korisniciTable.find('tr:gt(1)').remove();
				
				var rastuceSortKorIme = data.rastuceSortKorIme;
				for (it in rastuceSortKorIme) {
					korisniciTable.append(
						'<tr>' + 
						'<td><a href="PojedinacniKorisnik.html?id=' + rastuceSortKorIme[it].id + '">' + rastuceSortKorIme[it].korIme + '</a></td>' + 
							'<td>' + rastuceSortKorIme[it].datumRegistracije + '</td>' + 
							'<td>' + rastuceSortKorIme[it].uloga + '</td>' + 
						'</tr>'
					)
				}
			}
		});
	});
	
	$('#opadajuceSortKorIme').on('click', function(event) {
		getChecked();
		var korImeFilter = korImeFilterInput.val();
		var ulogaFilter = ulogaCB;
		
		var params = {
				'korImeFilter': korImeFilter, 
				'ulogaFilter': ulogaFilter
			};
		$.get('UpravljanjeKorisnicimaServlet', params, function(data){
			
			if (data.status == 'unauthenticated') {
				window.location.replace('Login.html');
				return;
			}

			if (data.status == 'success') {
				
				
				korisniciTable.find('tr:gt(1)').remove();
				
				var opadajuceSortKorIme = data.opadajuceSortKorIme;
				for (it in opadajuceSortKorIme) {
					korisniciTable.append(
						'<tr>' + 
						'<td><a href="PojedinacniKorisnik.html?id=' + opadajuceSortKorIme[it].id + '">' + opadajuceSortKorIme[it].korIme + '</a></td>' + 
							'<td>' + opadajuceSortKorIme[it].datumRegistracije + '</td>' + 
							'<td>' + opadajuceSortKorIme[it].uloga + '</td>' + 
						'</tr>'
					)
				}
			}
		});
	});
	
	$('#rastuceSortUloga').on('click', function(event) {
		getChecked();
		var korImeFilter = korImeFilterInput.val();
		var ulogaFilter = ulogaCB;

		var params = {
				'korImeFilter': korImeFilter, 
				'ulogaFilter': ulogaFilter
			};
		$.get('UpravljanjeKorisnicimaServlet', params, function(data){
			
			if (data.status == 'unauthenticated') {
				window.location.replace('Login.html');
				return;
			}

			if (data.status == 'success') {
				
				
				korisniciTable.find('tr:gt(1)').remove();
				
				var rastuceSortUloga = data.rastuceSortUloga;
				for (it in rastuceSortUloga) {
					korisniciTable.append(
						'<tr>' + 
						'<td><a href="PojedinacniKorisnik.html?id=' + rastuceSortUloga[it].id + '">' + rastuceSortUloga[it].korIme + '</a></td>' + 
							'<td>' + rastuceSortUloga[it].datumRegistracije + '</td>' + 
							'<td>' + rastuceSortUloga[it].uloga + '</td>' + 
						'</tr>'
					)
				}
			}
		});
	});
	
	$('#opadajuceSortUloga').on('click', function(event) {
		getChecked();
		var korImeFilter = korImeFilterInput.val();
		var ulogaFilter = ulogaCB;
		
		var params = {
				'korImeFilter': korImeFilter, 
				'ulogaFilter': ulogaFilter
			};
		$.get('UpravljanjeKorisnicimaServlet', params, function(data){
			
			if (data.status == 'unauthenticated') {
				window.location.replace('Login.html');
				return;
			}

			if (data.status == 'success') {
				
				
				korisniciTable.find('tr:gt(1)').remove();
				
				var opadajuceSortUloga = data.opadajuceSortUloga;
				for (it in opadajuceSortUloga) {
					korisniciTable.append(
						'<tr>' + 
						'<td><a href="PojedinacniKorisnik.html?id=' + opadajuceSortUloga[it].id + '">' + opadajuceSortUloga[it].korIme + '</a></td>' + 
							'<td>' + opadajuceSortUloga[it].datumRegistracije + '</td>' + 
							'<td>' + opadajuceSortUloga[it].uloga + '</td>' + 
						'</tr>'
					)
				}
			}
		});
	});
});