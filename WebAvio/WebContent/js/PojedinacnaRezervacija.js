var sedistaPolazniCB;
function getCheckedPolazni(){
	sedistaPolazniCB = document.getElementById("sedistaPolazniCB").value;	
}

var sedistaPovratniCB;
function getCheckedPovratni(){
	sedistaPovratniCB = document.getElementById("sedistaPovratniCB").value;	
}

$(document).ready(function() {

	var id = window.location.search.slice(1).split('&')[0].split('=')[1];
	console.log(id);
	
	function cbPolazni(){
		var sedistaPolazniCBa = document.getElementById('sedistaPolazniCB');
		$.get('PojedinacnaRezervacijaServlet',{'id':id}, function(data){
			var polazniSedista = data.polazniSedista;
			var i;	
			sedistaPolazniCBa.options[sedistaPolazniCBa.options.length] = new Option('-', 'None');	
			for (i = 0; i < polazniSedista.length; i++) { 
				sedistaPolazniCBa.options[sedistaPolazniCBa.options.length] = new Option(polazniSedista[i], polazniSedista[i]);	
			}	
			
		});
	}
	
	cbPolazni();
	
	function cbPovratni(){
		var sedistaPovratniCBa = document.getElementById('sedistaPovratniCB');
		$.get('PojedinacnaRezervacijaServlet',{'id':id}, function(data){
			var povratniSedista = data.povratniSedista;
			var i;	
			sedistaPovratniCBa.options[sedistaPovratniCBa.options.length] = new Option('-', 'None');	
			for (i = 0; i < povratniSedista.length; i++) { 
				sedistaPovratniCBa.options[sedistaPovratniCBa.options.length] = new Option(povratniSedista[i], povratniSedista[i]);	
			}	
			
		});
	}
	
	cbPovratni();
	function getRezervacijaKupljena(){
		$.get('PojedinacnaRezervacijaServlet', {'id': id}, function(data){
			console.log(data);
			if(data.status == 'unauthenticated'){
				window.location.replace('Login.html');
				return;
			}
			
			if(data.status == 'success'){
				var loggedInUser = data.loggedInUser;
				if(loggedInUser.blokiran === true){
					$('#izmenaDiv').hide();
					$('#kupovina').hide();
					$('#rezkupId').hide();
					
				}
				if(loggedInUser.uloga === 'ADMIN'){
					$('#adminParagraph').append('<a href="DodavanjeLeta.html"><i class="far fa-plus-square"></i> Dodavanje leta</a>');
					$('#adminParagraph').append('<a href="UpravljanjeKorisnicima.html"><i class="fas fa-users"></i> Upravljanje korisnicima</a>');
					$('#adminParagraph').append('<a href="Izvestavanje.html"><i class="fas fa-book"></i> Izvestavanje</a>');
				}
				var rez = data.rez;
				var lett = data.lett;
				if(rez.datumProdaje != null){
					$('#kupovina').hide();
					$('#izmenaDiv').hide();
					$('#deleteSubmit').hide();
				}
				
				$('#polazniLetCell').append('<a href="PojedinacniLet.html?id='+ rez.polazniLet +'">' + rez.polazniLet +'</a>')
				$('#povratniLetCell').append('<a href="PojedinacniLet.html?id='+ rez.povratniLet +'">' + rez.povratniLet +'</a>')
				if(rez.povratniLet == null){
					$('#povratniLetCell').text('/');
				}
				$('#sedistePolazniCell').text(rez.sedistePolazniLet);
				$('#sedistePovratniCell').text(rez.sedistePovratniLet);
				if(rez.sedistePovratniLet == null){
					$('#sedistePovratniCell').text('/');
				}
				var korImeInputPutnik = $('#imePutnikaInput');
				var prezimeInputPutnik = $('#prezimePutnikaInput');
				
				
				
				korImeInputPutnik.val(rez.imePutnika);
				prezimeInputPutnik.val(rez.prezimePutnika);
		
				
				
				
				
				
				$('#datumRezCell').text(rez.datumRezervacije);
				if(rez.datumRezervacije == null){
					$('#datumRezCell').text('/');
				}
				$('#datumProdajeCell').text(rez.datumProdaje);
				if(rez.datumProdaje == null){
					$('#datumProdajeCell').text('/');
				}
				$('#korisnikCell').append('<a href="PojedinacniKorisnik.html?id='+ rez.korisnik.id +'">' + rez.korisnik.korIme +'</a>')
				$('#imePutnikaCell').text(rez.imePutnika);
				$('#prezimePutnikaCell').text(rez.prezimePutnika);
				$('#cenaCell').text(lett.cena);
				
				if(rez.povratniLet == null){
					$('#sedistePovratniTr').hide();
				}
				
				var imePutnikaInput = $('#imePutnikaInput');
				var prezimePutnikaInput = $('#prezimePutnikaInput');
				
				
				$('#updateSubmit').on('click', function(event){
					var sedistePolazni = sedistaPolazniCB;
					var sedistePovratni = sedistaPovratniCB;
					var imePutnika = imePutnikaInput.val();
					var prezimePutnika = prezimePutnikaInput.val();
					
					if(imePutnika == ""){
						alert("Morate uneti ime putnika! ");
						event.preventDefault();
						return false;
					}
					else if(prezimePutnika == ""){
						alert("Morate uneti prezime putnika! ");
						event.preventDefault();
						return false;
					}
					
					if(sedistePolazni == null){
						sedistePolazni = rez.sedistePolazniLet;
					}
					else if(sedistePolazni == 'None'){
						sedistePolazni = rez.sedistePolazniLet;
					}
					
					if(sedistePovratni == null){
						sedistePovratni = rez.sedistePovratniLet;
					}
					else if(sedistePovratni == 'None'){
						sedistePovratni = rez.sedistePovratniLet;
					}
					
					params = {
							'action': 'update',
							'id': id, 
							'sedistePolazni': sedistePolazni,
							'sedistePovratni': sedistePovratni,
							'imePutnika': imePutnika,
							'prezimePutnika': prezimePutnika
						};
					console.log(params);
					
					$.post('PojedinacnaRezervacijaServlet', params, function(data){
						if(data.status == 'unauthenticated'){
							window.location.replace('Login.html');
							return;
						}
						
						if(data.status == 'success'){
							var id = data.loggedInUserId;
							window.location.replace('LetoviKor.html');
						}
					});
					event.preventDefault();
					return false;
				});
				
				function pocetna(){
					$.get('LetoviKorServlet', function(data){
						
						if (data.status == 'unauthenticated') {
							window.location.replace('Login.html');
							return;
						}

						if (data.status == 'success') {
							var id = data.loggedInUserId;
							$('#pocetna').append('<a href="LetoviKor.html">Pocetna</a>');
						}
					});
				}
				pocetna();
				
				$('#deleteSubmit').on('click', function(event){
					params = {
							'action': 'delete',
							'id':id
					};
					console.log(params);
					$.post('PojedinacnaRezervacijaServlet', params, function(data){
						if (data.status == 'unauthenticated'){
							window.location.replace('Login.html');
							return;
						}
						
						if (data.status == 'success'){
							var id = data.loggedInUserId;
							window.location.replace('PojedinacniKorisnik.html?id='+ rez.korisnik.id +'');
							return;
						}
					});
					
					event.preventDefault();
					return false;
				});
				$('#kupovina').on('click', function(event){
					
					
					params = {
							'action': 'kupovinaKarte', 
							'id': id,
						};
					
					$.post('PojedinacnaRezervacijaServlet', params, function(data) {
						
						if (data.status == 'unauthenticated') {
							window.location.replace('Login.html');
							return;
						}
						
						if (data.status == 'success') {
							window.location.replace('PojedinacnaRezervacija.html?id='+ id +'');
						}
					});
					
					event.preventDefault();
					return false;
				});
			}
			
		});
		
	}
	
	getRezervacijaKupljena();
	
	$("#sedistaPolazniCB").on("change", getCheckedPolazni);
	$("#sedistaPovratniCB").on("change", getCheckedPovratni);
	
});