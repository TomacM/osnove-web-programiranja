$(document).ready(function() {
	
	cbUloga();
	
	var ulogaCB;
	function getChecked(){
		ulogaCB = document.getElementById("ulogaCB").value;	
	}

	var blokiranCB;
	function getCheckedBlokiran(){
		
		blokiranCB = document.getElementById("blokiranCB").value;	
	}
	function cbUloga(){
		var ulogaCB = document.getElementById('ulogaCB');
		ulogaCB.options[ulogaCB.options.length] = new Option('ADMIN', 'ADMIN');	
		ulogaCB.options[ulogaCB.options.length] = new Option('KORISNIK', 'KORISNIK');
	}
	
	cbUloga();
	
	function cbBlokiran(){
		var blokiranCB = document.getElementById('blokiranCB');
		blokiranCB.options[blokiranCB.options.length] = new Option('Ne', 'false');	
		blokiranCB.options[blokiranCB.options.length] = new Option('Da', 'true');
	}
	cbBlokiran();
	
	cbUloga();
	
	var id = window.location.search.slice(1).split('&')[0].split('=')[1];
	console.log(id);
	
	
	function pocetna(){
		$.get('LetoviKorServlet', function(data){
			
			if (data.status == 'unauthenticated') {
				window.location.replace('Login.html');
				return;
			}

			if (data.status == 'success') {
				var id = data.loggedInUserId;
				$('#pocetna').append('<a href="LetoviKor.html">Pocetna</a>');
			}
		});
	}
	pocetna();
	
	function getKorisnik(){
		getChecked();
		getCheckedBlokiran();
		$.get('PojedinacniKorisnikServlet', {'id': id}, function(data) {
			console.log(data);
			if (data.status == 'unauthenticated') {
				window.location.replace('Login.html');
				return;
			}
			
			if (data.status == 'success') {
				var korisnik = data.korisnik;
				
	
				
				if (data.loggedInUserRole == 'KORISNIK') {
					
					
					$('#updateSubmitA').hide();
					var textToFind = 'KORISNIK'
					var dd = document.getElementById('ulogaCB');
					for (var i = 0; i < dd.options.length; i++) {
					    if (dd.options[i].text === textToFind ) {
					    	console.log(dd.options[i].text+textToFind)
					        dd.selectedIndex = i;
					        break;
					    }
					}
					if(korisnik.blokiran=== false){
						var dd = document.getElementById('blokiranCB');
						for (var i = 0; i < dd.options.length; i++) {
						    if (dd.options[i].text === textToFind ) {
						    	console.log(dd.options[i].text+textToFind)
						        dd.selectedIndex = i;
						        break;
						    }
						}}else if(korisnik.blokiran === true){
								var dd = document.getElementById('blokiranCB');
								for (var i = 0; i < dd.options.length; i++) {
								    if (dd.options[i].text === textToFind ) {
								    	console.log(dd.options[i].text+textToFind)
								        dd.selectedIndex = i;
								        break;
								    }
								}
						}
					$('#adminDiv').hide();
					$('#putnikDiv').show();
					if(korisnik.blokiran === true){
						$('#zaBlokiraneH2').hide();
						$('#zaBlokiraneTable').hide();
						$('#updateSubmitP').hide();
						$('#blokiranCB').hide();
						$('#rezkupId').hide();
						document.getElementById("korImeInput").disabled = true;
						document.getElementById("lozinkaInput").disabled = true;
						
					}
					var korImeInput = $('#korImeInput');
					var lozinkaInput = $('#lozinkaInput');
					
					
					korImeInput.val(korisnik.korIme);
					lozinkaInput.val(korisnik.lozinka);
					
					document.getElementById("ulogaCB").disabled = true;
					document.getElementById("blokiranCB").disabled = true;
					document.getElementById("deleteSubmit").disabled = true;
					
					
					$('#korImeCellPutnik').text(korisnik.korIme);
					$('#datumRegCellPutnik').text(korisnik.datumRegistracije);
					$('#ulogaCellPutnik').text(korisnik.uloga);
					$('#blokiranCellPutnik').text(korisnik.blokiran);
					
					var korImeInputPutnik = $('#korImeInput');
					var lozinkaInputPutnik = $('#lozinkaInput');
					
					korImeInputPutnik.val(korisnik.korIme);
					lozinkaInputPutnik.val(korisnik.lozinka);
					
					$('#updateSubmitP').on('click', function(event) {
						var korImePutnik = korImeInputPutnik.val();
						var lozinkaPutnik = lozinkaInputPutnik.val();
						
						if(korImePutnik == ""){
							alert("Korisnicko ime ne moze ostati prazno! ");
							event.preventDefault();
							return false;
						}
						else if(lozinkaPutnik == ""){
							alert("Lozinka ne moze ostati prazna! ");
							event.preventDefault();
							return false;
						}
						
						params = {
								'action': 'updatePutnikov',
								'id': id, 
								'korImePutnik': korImePutnik,
								'lozinkaPutnik': lozinkaPutnik,
							};
						
						console.log(params);
						$.post('PojedinacniKorisnikServlet', params, function(data) {
							if (data.status == 'unauthenticated') {
								window.location.replace('Login.html');
								return;
							}

							if (data.status == 'success') {
								window.location.replace('LetoviKor.html');
								return;
							}
						});

						event.preventDefault();
						return false;
						
					});
					
				} else if (data.loggedInUserRole == 'ADMIN') {
					$('#putnikDiv').hide()
					$('#adminDiv').show();
					$('#korImeCell').text(korisnik.korIme);
					$('#datumRegCell').text(korisnik.datumRegistracije);
					$('#ulogaCell').text(korisnik.uloga);
					$('#blokiranCell').text(korisnik.blokiran);
					$('#updateSubmitP').hide();
				
					

					var korImeInput = $('#korImeInput');
					var lozinkaInput = $('#lozinkaInput');
					
					
					korImeInput.val(korisnik.korIme);
					lozinkaInput.val(korisnik.lozinka);
					
					
					$('#updateSubmitA').on('click', function(event) {
						var korIme = korImeInput.val();
						var lozinka = lozinkaInput.val();
						var uloga = ulogaCB;
						var blokiran = blokiranCB;
					
						if(korIme == ""){
							alert("Korisnicko ime ne moze ostati prazno! ");
							event.preventDefault();
							return false;
						}
						else if(lozinka == ""){
							alert("Lozinka ne moze ostati prazna! ");
							event.preventDefault();
							return false;
						}
						
						if(uloga == ""){
							uloga = korisnik.uloga;
						}
						
						
						params = {
								'action': 'updateAdminov',
								'id': id, 
								'korIme': korIme,
								'lozinka': lozinka,
								'uloga': uloga,
								'blokiran': blokiran,
							};
						
						console.log(params);
						$.post('PojedinacniKorisnikServlet', params, function(data) {
							if (data.status == 'unauthenticated') {
								window.location.replace('Login.html');
								return;
							}

							if (data.status == 'success') {
								window.location.replace('LetoviKor.html');
								return;
							}
						});

						event.preventDefault();
						return false;
					});
					$('#deleteSubmit').on('click', function(event) {
						params = {
							'action': 'delete',
							'id': id, 
						};
						console.log(params);
						$.post('PojedinacniKorisnikServlet', params, function(data) {
							if (data.status == 'unauthenticated') {
								window.location.replace('Login.html');
								return;
							}

							if (data.status == 'success') {
								window.location.replace('LetoviKor.html');
								return;
							}
						});

						event.preventDefault();
						return false;
					});
				}
			}
		});
		
	}
	
	$("#ulogaCB").on("change", getKorisnik);
	$("#blokiranCB").on("change", getKorisnik);
	
	var rezervacijeTable = $('#rezervacijeTable');
	var prodateTable = $('#prodateTable');
	
	function getRezervacije(){

		$.get('RezervacijeServlet', {'id': id}, function(data){
			
			if (data.status == 'unauthenticated') {
				window.location.replace('Login.html');
				return;
			}

			if (data.status == 'success') {
				
				rezervacijeTable.find('tr:gt(0)').remove();
				
				var rezervacijeKorisnik = data.rezervacijeKorisnik;
				for (it in rezervacijeKorisnik) {
					rezervacijeTable.append(
						'<tr>' + 
						'<td><a href="PojedinacnaRezervacija.html?id=' + rezervacijeKorisnik[it].id + '">' + rezervacijeKorisnik[it].datumRezervacije + '</a></td>' + 
						'</tr>'
					);
				}
				
			}
		});
	}
	
	function getProdate(){

		$.get('RezervacijeServlet', {'id': id}, function(data){
			
			if (data.status == 'unauthenticated') {
				window.location.replace('Login.html');
				return;
			}

			if (data.status == 'success') {
				
				prodateTable.find('tr:gt(0)').remove();
				
				var prodateKorisnik = data.prodateKorisnik;
				for (it in prodateKorisnik) {
					prodateTable.append(
						'<tr>' + 
						'<td><a href="PojedinacnaRezervacija.html?id=' + prodateKorisnik[it].id + '">' + prodateKorisnik[it].datumProdaje + '</a></td>' + 
						'</tr>'
					);
				}
				
			}
		});
	}
	
	getKorisnik();

	getRezervacije();
	getProdate();
});
