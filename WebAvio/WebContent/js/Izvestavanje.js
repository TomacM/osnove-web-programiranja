$(document).ready(function() {
	$('#rastuceSortNazivAero').hide();
	$('#opadajuceSortNazivAero').hide();
	$('#rastuceSortBrLetova').hide();
	$('#opadajuceSortBrLetova').hide();
	$('#rastuceSortBrProdKarata').hide();
	$('#opadajuceSortBrProdKarata').hide();
	$('#rastuceSortCenaProdKarata').hide();
	$('#opadajuceSortCenaProdKarata').hide();
	
	
	var izvestavanjeTable = $('#izvestavanjeTable');
	
	$.get('LetoviKorServlet',function(data){
		if (data.status == 'unauthenticated') {
			window.location.replace('Login.html');
			return;
		}
		if (data.loggedInUser.uloga != "ADMIN"){
			window.location.replace('LetoviKor.html');
			return;
		}
	});
	$('#pretrazi').on('click', function(event) {
	    var lowD = document.getElementById("lowDatumDFilterInput").value;
	    var lowT = document.getElementById("lowDatumTFilterInput").value;
	    var higD = document.getElementById("highDatumDFilterInput").value;
	    var highT= document.getElementById("highDatumTFilterInput").value;
	    
	    if(lowD=='' || lowT=='' || higD=='' || highT=='' || (lowD+" "+lowT)>(higD+" "+highT)){
	    	alert("Moraju sva polja biti popunjena i od>do");
	    }else{
		    var lowD = document.getElementById("lowDatumDFilterInput").value;
		    var lowT = document.getElementById("lowDatumTFilterInput").value;
		    var higD = document.getElementById("highDatumDFilterInput").value;
		    var highT= document.getElementById("highDatumTFilterInput").value;
		    
			var params = {
					'lowDatumDTFilter':lowD+" "+lowT+":"+"00",
					'highDatumDTFilter':higD+" "+highT+":"+"00", 
			}
			
			var letoviTable = $('#letoviTable');
			
			
			$.get('IzvestavanjeServlet',params, function(data){

				if (data.status == 'success') {
					
					letoviTable.find('tr:gt(1)').remove();
					
					var izvestavanje = data.izvestavanje;	
					var ukBrojLetova = 0;
					var ukBrojProdatihKar = 0;
					var ukCenaSvihProdatihKar = 0;
					for (it in izvestavanje) {
						ukBrojLetova += izvestavanje[it].brojLetova;
						ukBrojProdatihKar += izvestavanje[it].brojProdatihKarata;
						ukCenaSvihProdatihKar += izvestavanje[it].cena;
						letoviTable.append(
								'<tr>' + 
								'<td>' + izvestavanje[it].nazivAero + '</a></td>' + 
									'<td>' + izvestavanje[it].brojLetova + '</td>' + 
									'<td>' + izvestavanje[it].brojProdatihKarata + '</td>' + 
									'<td>' + izvestavanje[it].cena + '</td>' + 
								'</tr>'
							);
					}
					letoviTable.append(
							'<tr>' +
								'<td align="right">Ukupno:</td>' +
								'<td>'+ ukBrojLetova +'</td>' +
								'<td>'+ ukBrojProdatihKar +'</td>' +
								'<td>'+ ukCenaSvihProdatihKar +'</td>' +
							'</tr>'

						);
				}
			});

	    	
	    }
	    
		
	});
	
});