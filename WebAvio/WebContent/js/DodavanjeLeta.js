$(document).ready(function() {
	
	function cbPolazni(){
		polazniAeroFilterCB = document.getElementById('polazniAeroFilterCB');
		$.get('LetoviServlet', function(data){
			var polazniAerodromi = data.polazniAerodromi;
			var i;
			for (i = 0; i < polazniAerodromi.length; i++) { 
				polazniAeroFilterCB.options[polazniAeroFilterCB.options.length] = new Option(polazniAerodromi[i], polazniAerodromi[i]);	
			}	
			
		});
	}
	cbPolazni();
	
	function cbDolazni(){
		dolazniAeroFilterCB = document.getElementById('dolazniAeroFilterCB');
		$.get('LetoviServlet', function(data){
			var dolazniAerodromi = data.dolazniAerodromi;
			var i;
			for (i = 0; i < dolazniAerodromi.length; i++) { 
				dolazniAeroFilterCB.options[dolazniAeroFilterCB.options.length] = new Option(dolazniAerodromi[i], dolazniAerodromi[i]);	
			}	
			
		});
	}

	cbDolazni();
	
	function pocetna(){
		$.get('LetoviKorServlet', function(data){
			
			if (data.status == 'unauthenticated') {
				window.location.replace('Login.html');
				return;
			}

			if (data.status == 'success') {
				var id = data.loggedInUserId;
				$('#pocetna').append('<a href="LetoviKor.html">Pocetna</a>');
			}
		});
	}
	pocetna();
	 
	var brojInput = $('#brojInput');
	var brojSedistaInput = $('#brojSedistaInput');
	var cenaInput = $('#cenaInput');
	

	$('#letSubmit').on('click', function(event) {
		var broj = brojInput.val();
		var brojSedista = brojSedistaInput.val();
		var cena = cenaInput.val();
	    var datumPolaska = document.getElementById("DatumPolaskaDFilterInput").value;
	    var vremePolaska = document.getElementById("DatumPolaskaTFilterInput").value;
	    var datumDolaska = document.getElementById("DatumDolaskaDFilterInput").value;
	    var vremeDolaska= document.getElementById("DatumDolaskaTFilterInput").value;
	    
	    var e = document.getElementById("polazniAeroFilterCB");
		var polazni = e.options[e.selectedIndex].text;
		
		var b = document.getElementById("dolazniAeroFilterCB");
		var dolazni = b.options[b.selectedIndex].text;
		
		console.log('broj'+broj)
		console.log('brojSedista'+brojSedista)
		console.log('cena'+cena)
		console.log('datumPolaska'+datumPolaska)
		console.log('vremePolaska'+vremePolaska)
		console.log('datumDolaska'+datumDolaska)
		console.log('vremeDolaska'+vremeDolaska)
		console.log('polazni'+polazni)
		console.log('dolazni'+dolazni)
		
	    
	    if(datumPolaska=='' || vremePolaska=='' || datumDolaska=='' || vremeDolaska=='' || (datumPolaska+" "+vremePolaska)>(datumDolaska+" "+vremeDolaska)){
	    	alert("Moraju sva polja biti popunjena i Datum dolaska > Datum polaska");
			event.preventDefault();
			return;
	    }else if(broj == ""){
			alert('Broj leta ne sme biti prazan! ');
			event.preventDefault();
			return;
		}else if(broj.length>4){
			alert('Broj leta mora biti manji od 9999! ');	
			event.preventDefault();
			return;
		}else if(isNaN(broj)){
			alert('Broj leta mora biti broj! ');	
			event.preventDefault();
			return;
		}else if(brojSedista == ""){
			alert('Broj sedista ne sme biti prazan! ');
			event.preventDefault();
			return;
		}else if(isNaN(brojSedista)){
			alert('Broj sedista mora biti broj! ');	
			event.preventDefault();
			return;
		}else if(brojSedista.length>2){
			alert('Broj sedista mora biti manji od 99! ');	
			event.preventDefault();
			return;
		}else if(cena == ""){
			alert('Cena ne sme biti prazana! ');
			event.preventDefault();
			return;
		}else if(isNaN(cena)){
			alert('Cena sedista mora biti broj! ');	
			event.preventDefault();
			return;
		}else if(cena.length>5){
			alert('Cena mora biti manji od 99999! ');	
			event.preventDefault();
			return;
		}else if(polazni === dolazni){
			alert('Dolazni aerodrom se mora razlikovati od polaznog aerodroma! ');
			event.preventDefault();
			return;
		}
	    
		params = {
				'action': 'add',
				'broj': broj,
				'datumPolaska':datumPolaska+" "+vremePolaska+":"+"00",
				'datumDolaska':datumDolaska+" "+vremeDolaska+":"+"00", 
				'polazniAerodrom': polazni,
				'dolazniAerodrom': dolazni,
				'brojSedista': brojSedista,
				'cena': cena
			};
		
		
		$.post('PojedinacniLetServlet', params, function(data) {
			if (data.status == 'unauthenticated') {
				window.location.replace('Login.html');
				return;
			}

			if (data.status == 'success') {
				var id = data.loggedInUserId;
				window.location.replace('LetoviKor.html');
				return;
			}
		});

		event.preventDefault();
		return false;
	});
			
	
});