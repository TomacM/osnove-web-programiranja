$(document).ready(function() {
	var id = window.location.search.slice(1).split('&')[0].split('=')[1];
	console.log(id);
	var a = document.getElementById('rezervacija');
	 a.setAttribute('href', "Faza2.html?id="+id);
	var params = {
			'id': id
	};
	


	
	function funkcija(){
		$.get('UlogaKorisnikaServlet', {'action': 'loggedInUserRole'}, function(data) {
			console.log(data);

			if (data.status == 'unauthenticated') {
				window.location.replace('Login.html');
				return;
			}

			if (data.status == 'success') {
				if (data.loggedInUserRole == 'ADMIN') {
					$('#adminForm').show();
					$('#deleteSubmit').show();
					$('#rezervacija').hide();
				}if (data.loggedInUserRole == 'KORISNIK') {
					$('#adminForm').hide();
					$('#deleteSubmit').hide();
					$('#prikaz').hide();
				}
			}
		});
	}
	funkcija();
	
	function pocetna(){
		$.get('LetoviKorServlet', function(data){
			
			if (data.status == 'unauthenticated') {
				window.location.replace('Login.html');
				return;
			}

			if (data.status == 'success') {
				var id = data.loggedInUserId;
				$('#pocetna').append('<a href="LetoviKor.html">Pocetna</a>');
			}
		});
	}
	pocetna();
	
	$.get('PojedinacniLetServlet',params, function(data){
		var lett = data.lett;
		var slobodnaSedista = data.slobodnaSedista;
		
		$('#brojCell').text(String(lett.broj));
		$('#datumPolaskaCell').text(String(lett.datumPolaska));
		$('#datumDolaskaCell').text(String(lett.datumDolaska));
		$('#polazniAerodromCell').text(String(lett.polazniAerodrom));
		$('#dolazniAerodromCell').text(String(lett.dolazniAerodrom));
		$('#brojSedistaCell').text(String(lett.brojSedista));
		$('#brojSlobodnihSedistaCell').text(String(slobodnaSedista));
		$('#cenaCell').text(lett.cena);
		
		var d = new Date
		
		var godina=d.getFullYear()
		var mesec= d.getMonth()+1
		var dan = d.getDate()
		var mesec1 ="0"+mesec
		var sat = d.getHours()
		var minut = d.getMinutes()
		var sekund = d.getSeconds()
		
		var format =godina+"-"+mesec1+"-"+dan+" "+sat+":"+minut+":"+sekund
		console.log(format)
		console.log(format+"<"+lett.datumPolaska)
		
		
		
		
		if(format>lett.datumPolaska){
			  var link=document.getElementById("rezervacija")
			  link.href = "javascript:void(0)";
		}
		var korisnik = data.loggedInUser
		if(korisnik.blokiran===true){
			  var link=document.getElementById("rezervacija")
			  link.href = "javascript:void(0)";
		}
			
		
		$('#datumPolaskaInput').val(String(lett.datumPolaska));
		$('#datumDolaskaInput').val(String(lett.datumDolaska));
		$('#brojSedistaInput').val(String(lett.brojSedista));
		$('#cenaInput').val(lett.cena);

	});
	

	
	
	

	function cbPolazni(){
		polazniAeroFilterCB = document.getElementById('polazniAeroFilterCB');
		$.get('LetoviServlet', function(data){
			var polazniAerodromi = data.polazniAerodromi;
			var i;
			for (i = 0; i < polazniAerodromi.length; i++) { 
				polazniAeroFilterCB.options[polazniAeroFilterCB.options.length] = new Option(polazniAerodromi[i], polazniAerodromi[i]);	
			}	
			
		});
	}
	cbPolazni();
	
	function selektovanje1(){
		$.get('PojedinacniLetServlet',params, function(data){
			var lett = data.lett;
			var textToFind = String(lett.polazniAerodrom) 
			var dd = document.getElementById('polazniAeroFilterCB');
			for (var i = 0; i < dd.options.length; i++) {
			    if (dd.options[i].text === textToFind ) {
			    	console.log(dd.options[i].text+textToFind)
			        dd.selectedIndex = i;
			        break;
			    }
			}
			});
		}
		 selektovanje1();
		 
	
	function cbDolazni(){
		dolazniAeroFilterCB = document.getElementById('dolazniAeroFilterCB');
		$.get('LetoviServlet', function(data){
			var dolazniAerodromi = data.dolazniAerodromi;
			var i;
			for (i = 0; i < dolazniAerodromi.length; i++) { 
				dolazniAeroFilterCB.options[dolazniAeroFilterCB.options.length] = new Option(dolazniAerodromi[i], dolazniAerodromi[i]);	
			}	
			
		});
	}

	cbDolazni();
	
	
	function selektovanje2(){
		$.get('PojedinacniLetServlet',params, function(data){
			var lett = data.lett;
			var textToFind = String(lett.dolazniAerodrom)
			var bb = document.getElementById('dolazniAeroFilterCB');
			for (var i = 0; i < bb.options.length; i++) {
			    if (bb.options[i].text === textToFind ) {
			      	console.log(bb.options[i].text+textToFind)
			        bb.selectedIndex = i;
			        break;
			    }
			}
			});
		}
	selektovanje2();

	selektovanje1();
	selektovanje2();
	selektovanje1();
	selektovanje2();

	selektovanje1();
	selektovanje2();
	
	$('#deleteSubmit').on('click', function(event) {
		if (confirm('Da li ste sigurni da zelite da obrisete let?')) {
			params = {
					'action': 'delete',
					'id': id
				};
				console.log(params);
				$.post('PojedinacniLetServlet', params, function(data) {
					if (data.status == 'unauthenticated') {
						window.location.replace('Login.html');
						return;
					}

					if (data.status == 'success') {
						var id = data.loggedInUserId;
						window.location.replace('LetoviKor.html');
						return;
					}
				});

				event.preventDefault();
				return false;
		} else {
			location.reload(); 
		}
	});
	
	$('#updateSubmit').on('click', function(event) {
		re = /([0-2][0-9]{3})-([0-1][0-9])-([0-3][0-9]) ([0-5][0-9]):([0-5][0-9]):([0-5][0-9])(([\-\+]([0-1][0-9])\:00))?/;
		var datumPolaska = document.getElementById('datumPolaskaInput').value;
		var datumDolaska = document.getElementById('datumDolaskaInput').value;
		var cena = document.getElementById('cenaInput').value;
		var brojSedista = document.getElementById('brojSedistaInput').value;
		var e = document.getElementById("polazniAeroFilterCB");
		var polazni = e.options[e.selectedIndex].text;
		
		var b = document.getElementById("dolazniAeroFilterCB");
		var dolazni = b.options[b.selectedIndex].text;
		
		if(datumDolaska == ""){
			alert('Datum dolaska ne sme biti prazan! ');
			event.preventDefault();
			return;
		} 
		else if(!datumDolaska.match(re)){
			alert('Datum dolaska nije u dobrom formatu (YYYY-MM-DD hh:mm:ss)! ');
			event.preventDefault();
			return;
		}
		else if(datumDolaska <= datumPolaska){
			alert('Datum dolaska mora biti nakon datuma polaska! ');
			event.preventDefault();
			return;
		}else if(datumPolaska == ""){
			alert('Datum polaska ne sme biti prazan! ');
			event.preventDefault();
			return;
		} 
		else if(!datumPolaska.match(re)){
			alert('Datum polaska nije u dobrom formatu (YYYY-MM-DD hh:mm:ss)! ');
			event.preventDefault();
			return;
		}
		else if(datumDolaska <= datumPolaska){
			alert('Datum polaska mora biti nakon datuma polaska! ');
			event.preventDefault();
			return;
		}else if(polazni == ""){
			alert('Polazni aerodrom ne moze biti prazan! ');
			event.preventDefault();
			return;	
		}	else if(dolazni == ""){
			alert('Dolazni aerodrom ne moze biti prazan! ');
			event.preventDefault();
			return;
		}else if(polazni === dolazni){
			alert('Dolazni aerodrom se mora razlikovati od polaznog aerodroma! ');
			event.preventDefault();
			return;
		}else if(brojSedista == ""){
			alert('Broj sedista ne sme biti prazan! ');
			event.preventDefault();
			return;
		}
		else if(isNaN(brojSedista)){
			alert('Broj sedista mora biti broj! ');	
			event.preventDefault();
			return;
		}
		else if(cena == ""){
			alert('Cena ne sme biti prazna! ');
			event.preventDefault();
			return;
		}
		else if(isNaN(cena)){
			alert('Cena mora biti broj! ');	
			event.preventDefault();
			return;
		}
		params = {
				'action': 'update',
				'id': id,
				'datumPolaska': datumPolaska,
				'datumDolaska': datumDolaska,
				'polazniAerodrom': polazni,
				'dolazniAerodrom': dolazni,
				'brojSedista': brojSedista,
				'cena': cena
			};
		
		console.log('id: '+id);

		$.post('PojedinacniLetServlet', params, function(data) {
			if (data.status == 'unauthenticated') {
				window.location.replace('Login.html');
				return;
			}

			if (data.status == 'success') {
				var id = data.loggedInUserId;
				window.location.replace('LetoviKor.html');
				return;
			}
		});

		event.preventDefault();
		return false;
	});
	
	
	var rezervacijeTable = $('#rezervacijeTable');
	var prodateTable = $('#prodateTable');
	
	function getRezervacije(){

		$.get('RezervacijeServlet', {'id': id}, function(data){
			
			if (data.status == 'unauthenticated') {
				window.location.replace('Login.html');
				return;
			}

			if (data.status == 'success') {
				
				rezervacijeTable.find('tr:gt(0)').remove();
				
				var rezervacijePolazni = data.rezervacijePolazni;
				var rezervacijePovratni = data.rezervacijePovratni;
				for (it in rezervacijePolazni) {
					rezervacijeTable.append(
						'<tr class="trtr">' + 
						'<td><a href="PojedinacnaRezervacija.html?id=' + rezervacijePolazni[it].id + '">' + rezervacijePolazni[it].datumRezervacije + '</a></td>' + 
							'<td>' + rezervacijePolazni[it].sedistePolazniLet + '</td>' + 
							'<td/>' + 
							'<td><a href="PojedinacniKorisnik.html?id=' + rezervacijePolazni[it].korisnik.id + '">' + rezervacijePolazni[it].korisnik.korIme + '</td>' + 
						'</tr>'
					);
				}
				
				for (itp in rezervacijePovratni){
					rezervacijeTable.append(
							'<tr class="trtr">' + 
							'<td><a href="PojedinacnaRezervacija.html?id=' + rezervacijePovratni[itp].id + '">' + rezervacijePovratni[itp].datumRezervacije + '</a></td>' + 
								'<td/>' + 
								'<td>' + rezervacijePovratni[itp].sedistePovratniLet + '</td>' + 
								'<td><a href="PojedinacniKorisnik.html?id=' + rezervacijePovratni[itp].korisnik.id + '">' + rezervacijePovratni[itp].korisnik.korIme + '</td>' + 
							'</tr>'
					);
				}
			}
		});
		
	}
	
	function getProdate(){

		$.get('RezervacijeServlet', {'id': id}, function(data){
			
			if (data.status == 'unauthenticated') {
				
				window.location.replace('Login.html');
				return;
			}

			if (data.status == 'success') {
				
				prodateTable.find('tr:gt(0)').remove();
				
				var prodatePolazni = data.prodatePolazni;
				var prodatePovratni = data.prodatePovratni;
				for (it in prodatePolazni) {
					prodateTable.append(
						'<tr class="trtr">' + 
						'<td><a href="PojedinacnaRezervacija.html?id=' + prodatePolazni[it].id + '">' + prodatePolazni[it].datumProdaje + '</a></td>' + 
							'<td>' + prodatePolazni[it].sedistePolazniLet + '</td>' + 
							'<td/>' + 
							'<td><a href="PojedinacniKorisnik.html?id=' + prodatePolazni[it].korisnik.id + '">' + prodatePolazni[it].korisnik.korIme + '</td>' + 
						'</tr>'
					);
				}
				
				for (itp in prodatePovratni){
					prodateTable.append(
							'<tr class="trtr">' + 
							'<td><a href="PojedinacnaRezervacija.html?id=' + prodatePovratni[itp].id + '">' + prodatePovratni[itp].datumProdaje + '</a></td>' + 
								'<td/>' + 
								'<td>' + prodatePovratni[itp].sedistePovratniLet + '</td>' + 
								'<td><a href="PojedinacniKorisnik.html?id=' + prodatePovratni[itp].korisnik.id + '">' + prodatePovratni[itp].korisnik.korIme + '</td>' + 
							'</tr>'
					);
				}
			}
		});
	}
	
	getRezervacije();
	getProdate();

});